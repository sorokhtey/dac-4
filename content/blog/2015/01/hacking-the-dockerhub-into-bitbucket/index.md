---
title: "Hacking the DockerHub into Bitbucket"
date: "2015-01-14"
author: "npaolucci"
categories: ["docker", "bitbucket", "hack", "shipit"]
---

Today, I want to tell you about the results of a hackathon, or as we call it a
[ShipIt][2] project. It's fun for me to share and I hope you'll find it
interesting. At Atlassian we have a big culture of innovation and
experimentation. Every quarter, the company stops for 24 hours and employees
can pick their own project to scratch their own itch: they form groups, sprint
and spike working on new ideas. Sometimes we work on things completely off the
wall, sometimes tiny improvements. Several key features included in our
products (or even entirely new products) came out of these sprints of
innovations.

<img src="shipit.png" alt="shipit" style="margin-top: 10px;margin-bottom: 10px;" />

The ShipIt time is always very exciting for me. A few months back I was in San
Francisco and I paired up with a couple of colleagues on a Docker related
project, as you might know or not know, I have been very excited about the
technology and I have been talking about it a lot.

So here's my idea: the Docker registry collects a lot of software packages and
or ready-made images. We already have a cool integration with the Docker
registry in the sense that you can set-up the Docker registry to rebuild your
image anytime you push something to Bitbucket.

<img src="integration.png" alt="integration" style="margin-top: 10px;margin-bottom: 10px; border: 1px solid gray;" />

It would be nice if we could show the state of the images and also the
interaction with the Docker community on [Bitbucket][3] itself. So what I
needed to develop a solution was:

- A simple static website using only HTML, JavaScript and CSS. 
- Where the JavaScript part was done with [React][4] and [Flux][5].
- [nginx][6] both for serving the static content and as proxy to avoid [Cross-origin resource sharing][7] restrictions.
- [Docker][8] containers and [fig][9] to package and run the application.

But enough teasing! Have a look at the video for the (then live) DEMO and an
extended explanation of the Docker setup used.

### Video

<iframe width="850" height="500" src="//www.youtube.com/embed/XecmBWXwZ6Y" frameborder="0" allowfullscreen></iframe>

[Full transcription][1]

[1]: transcription.txt
[2]: https://www.atlassian.com/company/about/shipit?utm_source=DAC&utm_medium=blog&utm_campaign=dockerhub
[3]: http://bitbucket.org/?utm_source=DAC&utm_medium=blog&utm_campaign=dockerhub
[4]: http://facebook.github.io/react/
[5]: http://facebook.github.io/flux/
[6]: http://nginx.org/
[7]: http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
[8]: https://www.docker.com/
[9]: http://www.fig.sh/
