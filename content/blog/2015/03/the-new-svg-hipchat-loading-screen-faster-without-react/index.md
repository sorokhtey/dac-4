---
title: "The new SVG HipChat loading screen - faster without React"
date: "2015-03-17"
author: "delkan"
categories: ["react", "css", "svg", "HipChat", "javascript"]
---
<svg height="0" style="display:block;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <symbol id="icon-hipchat-logo" viewBox="0 0 1024 1024">
    <title>hipchat-logo</title>
    <path class="path1" d="M791.552 574.464c0-10.24-8.192-20.48-20.48-20.48-5.12 0-9.216 2.048-13.312 5.12-51.2 46.080-139.264 87.040-245.76 88.064v0c-105.472 0-191.488-43.008-245.76-88.064-3.072-3.072-10.24-5.12-14.336-5.12-12.288 0-19.456 10.24-19.456 22.528 0 9.216 6.144 16.384 11.264 24.576 30.72 46.080 124.928 122.88 267.264 122.88h2.048c142.336 0 236.544-77.824 267.264-122.88 5.12-8.192 11.264-18.432 11.264-26.624z"></path>
    <path class="path2" d="M932.864 929.792c-35.84-20.48-70.656-55.296-86.016-103.424-3.072-9.216 0-17.408 7.168-22.528 104.448-79.872 169.984-197.632 169.984-328.704 0-243.712-229.376-441.344-512-441.344s-512 197.632-512 441.344c0 243.712 229.376 441.344 512 441.344 36.864 0 72.704-3.072 107.52-10.24 8.192-2.048 15.36 0 21.504 4.096 69.632 43.008 174.080 78.848 266.24 78.848 29.696 0 40.96-18.432 40.96-35.84-1.024-9.216-6.144-18.432-15.36-23.552zM795.648 886.784c2.048 2.048 2.048 4.096 2.048 6.144 0 3.072-3.072 5.12-6.144 5.12-25.6 0-104.448-43.008-146.432-77.824-6.144-5.12-13.312-6.144-24.576-4.096-34.816 7.168-70.656 11.264-108.544 11.264-235.52 0-425.984-157.696-425.984-352.256s190.464-352.256 425.984-352.256c235.52 0 425.984 157.696 425.984 352.256 0 115.712-67.584 219.136-173.056 282.624-7.168 4.096-14.336 12.288-14.336 22.528 0 21.504 19.456 72.704 45.056 106.496z"></path>
  </symbol>
</svg>

<style>

  #icon-hipchat-logo .path1,
  #icon-hipchat-logo .path2 {
    fill: #205081;
  }

  .loading-screen {
    padding-top: 40px;
    text-align: center;
  }

  .loading-screen svg,
  .loading-message svg {
    height: 100px;
    width: 100px;
  }

  .loading-message {
    align-items: center;
    background-color: #fff;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: column;
    flex-flow: column;
    height: 200px;
    -webkit-justify-content: center;
    justify-content: center;
    text-align: center;
    width: 100%;
  }

  .loading-message div.description {
    margin-top: 15px;
  }

  .loading-message:hover .loading-outer {
    -webkit-animation: translationFrames linear 0.66s infinite;
    -webkit-transform-origin: 50% 50%;
    animation: translationFrames linear 0.66s infinite;
    transform-origin: 50% 50%;
  }
  .loading-message:hover  .loading-inner {
    -webkit-animation: rotationFrames linear 0.66s infinite;
    -webkit-transform-origin: 50% 50%;
    animation: rotationFrames linear 0.66s infinite;
    transform-origin: 50% 50%;
  }

  @keyframes translationFrames {
    0% {
      transform: translate(0, 3px);
    }
    14% {
      transform: translate(0, 1px);
    }
    24% {
      transform: translate(0, 6px);
    }
    28% {
      transform: translate(0, 7px);
    }
    30% {
      transform: translate(0, 3px);
    }
    42% {
      transform: translate(0, -7px);
    }
    46% {
      transform: translate(0, -8px);
    }
    53% {
      transform: translate(0, -9px);
    }
    73% {
      transform: translate(0, -9px);
    }
    83% {
      transform: translate(0, -3px);
    }
    100% {
      transform: translate(0, 3px);
    }
  }

  @-webkit-keyframes translationFrames {
    0% {
      -webkit-transform: translate(0, 3px);
    }
    14% {
      -webkit-transform: translate(0, 1px);
    }
    24% {
      -webkit-transform: translate(0, 6px);
    }
    28% {
      -webkit-transform: translate(0, 7px);
    }
    30% {
      -webkit-transform: translate(0, 3px);
    }
    42% {
      -webkit-transform: translate(0, -7px);
    }
    46% {
      -webkit-transform: translate(0, -8px);
    }
    53% {
      -webkit-transform: translate(0, -9px);
    }
    73% {
      -webkit-transform: translate(0, -9px);
    }
    83% {
      -webkit-transform: translate(0, -3px);
    }
    100% {
      -webkit-transform: translate(0, 3px);
    }
  }

  @keyframes rotationFrames {
    0% {
      transform: rotate(-180deg);
    }
    30% {
      transform: rotate(0);
    }
    42% {
      transform: rotate(20deg);
    }
    57% {
      transform: rotate(38deg);
    }
    71% {
      transform: rotate(55deg);
    }
    79% {
      transform: rotate(70deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }

  @-webkit-keyframes rotationFrames {
    0% {
      -webkit-transform: rotate(-180deg);
    }
    30% {
      -webkit-transform: rotate(0);
    }
    42% {
      -webkit-transform: rotate(20deg);
    }
    57% {
      -webkit-transform: rotate(38deg);
    }
    71% {
      -webkit-transform: rotate(55deg);
    }
    79% {
      -webkit-transform: rotate(70deg);
    }
    100% {
      -webkit-transform: rotate(180deg);
    }
  }

</style>

We love gifs on the HipChat team. We love them so much that we were temporarily
misled by their sirens' call. Here's how we plugged our ears and continued on to
the fabled land of SVG, optimizing our loading screen and reducing its size by
95%.

## Fatty the gif and the loading screen

Our original loading screen was a thing of beauty. A pure white screen sporting
an animated, retina HipChat logo rolling over its tail into the future and
beyond.

<div class="loading-screen">
    <img alt="A hidden, pre-loader HipChat loading gif" src="load_0.gif" width="100" height="100" style="display:none">
    <img alt="The old HipChat loading gif" src="load_0.png" width="100" height="100" onmouseover="this.src='load_0.gif'" onmouseout="this.src='load_0.png'">
    <div class="description">Roll over to animate</div>
</div>

This retina gif's size (a chunky 53KB) was the source of its demise. It was
simply too big for a loading screen, often arriving late and missing the party
entirely.

Size aside, the old loading screen was a part of the greater web app. This meant
it would not appear until [React](http://facebook.github.io/react/) and all of
the other dependencies of the web client were loaded. That sounds
counter-intuitive, but when we first launched the beta, the javascript payload
was smaller. The loading screen was simply used to hide the UI whilst we
fetched the initial HipChat session data. Since then we've added countless
features, like inline file previews, increasing the download size.

We've gone one better. The loading screen now loads before
React. Infact, the SVG HipChat logo and
animation CSS are inline in the HTML, so there's no additional requests to make
to show the loading screen.

## Captivating new users

New users are precious and first impressions last. If you have to keep them
waiting, captivate them. Quickly.

After our initial web client beta release, we conducted an intense study of the
new user on-boarding flow. It was then that we discovered our gif issue. In
production, new users with an empty cache on a typical internet connection,
often didn't see our fancy loading gif at all.

## Farewell gif. Hello SVG.

Our new web client already uses [SVG symbol definitions](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/symbol)
for our status icons, so it was a no-brainer to add a HipChat logo along-side
them.

With this HTML and CSS:
``` html
<svg height="0" style="display:block;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <symbol id="icon-hipchat-logo" viewBox="0 0 1024 1024">
    <title>hipchat-logo</title>
    <path class="path1" d="M791.552 ..."></path>
    <path class="path2" d="M932.864 ..."></path>
  </symbol>
</svg>
```
``` css
#icon-hipchat-logo .path1, #icon-hipchat-logo .path2 {
  fill: #205081;
}
```
It looks a little like this:

<div class="loading-screen">
  <svg><use xlink:href='#icon-hipchat-logo'></use></svg>
</div>

## Let's dance!

Advanced CSS animations are tricky. The animation loop needed to account for the
logo's tail striking the ground. Piecing something intricate like this together
with the provided transition types (ease-in, ease-out, etc) is a great recipe
for a self-induced lobotomy. Instead, thanks to
<a href="http://twitter.com/joelunger">Joel Unger</a> on the design team, I was
able to create rotation and translation [keyframe](https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes)
rules from the original After Effects timeline.

<img alt="Original After Effects keyframes" src="aa-keyframes.png">

The loop is 49 frames at 29.97fps giving us a total length of 0.6 seconds. From
here we can create percentage-based keyframes.
i.e. At frame 22 (46% through the loop), when the tail hits the ground, we want
to apply a rotation of 20 degrees and translation of 7 pixels up.

** *Please note this snippet does not include browser-prefixes currently
required to work in all browsers.**

``` css
.loading-message .loading-outer {
  animation: translationFrames linear 0.66s infinite;
  transform-origin: 50% 50%;
}

.loading-message .loading-inner {
  animation: rotationFrames linear 0.66s infinite;
  transform-origin: 50% 50%;
}

@keyframes translationFrames {
  42% {
    transform: translate(0, -7px);
  }
}

@keyframes rotationFrames {
  42% {
    transform: rotate(20deg);
  }
}
```

## The result

From a 53KB gif to 1.8KB of HTML and CSS (both sizes gzipped).

<div class="loading-message">
  <div class="loading-outer">
    <div class="loading-inner">
      <svg><use xlink:href='#icon-hipchat-logo'></use></svg>
    </div>
  </div>
  <div class="description">Roll over to animate</div>
</div>

You can find all of the code for this CSS animation by viewing the source of
this blog post (or by [signing up for a HipChat account](https://www.hipchat.com/sign_up)).
