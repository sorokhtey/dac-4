---
title: "Maximize your next conference with these tips"
date: "2015-04-21T16:00:00+07:00"
author: "rwhitbeck"
categories: ["events", "conferences", "AtlasCamp"]
---

<style type="text/css">
.aui-page-panel img {
	margin: 20px auto;
	text-align: center;
	border: 1px solid #ccc;
	display: block;
}
.lede {
	display: none;
	visibility: hidden;
	font-style: italic;
}
.blog-archive .lede {
	display: block;
	visibility: visible;
	font-style: normal;
}
.blog-archive img {
	margin: 0;
}
</style>

<p class="lede">AtlasCamp is fast approaching, June 9-11 in Prague, Czech Republic. 
I'm looking forward to meeting new people, learning new techniques and building new 
partnerships. I wanted to offer my experiences in how I make the most out of developer 
conferences.</p>

<img src="atlascamp.png" alt="AtlasCamp 2015">

[AtlasCamp](http://atlassian.com/atlascamp) is fast approaching, June 9-11 in
 Prague, Czech Republic.  AtlasCamp is our developer Super Bowl, World Cup, Stanley Cup, 
 or whatever sports metaphor that works best. I'm looking forward to meeting 
 new people, learning new techniques and building new partnerships. I wanted 
 to offer my experiences in how I make the most out of developer conferences.
 Here are a few of my suggestions for getting the greatest value out of a conference.

## Prioritizing your time

At any conference your time will compete with many activities. How can you maximize 
your time while you're there? Here are some things to consider before you arrive. 

* Are there many tracks going on at once?  
* Will you have access to video recordings after the conference? 
* Will there be a lounge or hack area outside the sessions?

Check the conference web site for answers. You may need to contact the organizers
 to determine these answers.  The organizers may not have considered answers 
 to these questions yet. To show that there is demand you should contact them early.

The most important question to answer is the availability of video recordings.  
Without video recordings attending the sessions is the priority. With video 
recordings, attending the sessions is secondary. Video frees you to talk, hack, 
build relationships with others in the "hallway track." A lounge or hack area 
will make the "hallway track" more enticing to others as well.

<img src="prioritize.jpg" alt="The Hallway Track">

With video, you can take the choice out of which session to attend.  You can stay 
put in one track and catch the other track later on.  Without video you'll need to 
plan ahead and choose the session that is best.

At AtlasCamp, we will have two tracks this year. Track 1 will center around add-on 
development and track 2 on developer tools. We will record each session and they 
will be available after the conference.  Finally, we will have a network lounge 
and developer breakouts for attendees.

## Attend the Keynote

The one session you should not miss is the keynote. All conferences have a keynote. 
It dictates the rest of the conference with high profile speakers or major announcements. 
Yet, the keynote is the single common session for the entire conference. You can use 
the keynote to break the ice while networking. 

<img src="mcb.jpg" alt="Mike Cannon-Brookes - Keynote Speaker">

For AtlasCamp, we'll have Atlassian co-founder Mike Cannon-Brookes giving the 
keynote address. He'll sure to have great information about Atlassian products 
that you'll talk about later.

## Network the Parties

There should be at least one networking event or after party to meet others. Make sure 
you go and bring lots of business cards and a pen, to jot a note about the person you 
just met. Then go out and talk to people.  Join in their conversations, listen and ask 
questions. Learning how to network and connect with other people is a great skill to learn. 
Dale Carnegie's book, _[How to Win Friends and Influence People](http://www.amazon.com/How-Win-Friends-Influence-People-ebook/dp/B003WEAI4E)_ 
defines those skills. He provides a few simple rules (that are covered more in-depth in the 
book) that apply:

### Fundamental techniques 

1. Don't criticize, condemn or complain.
1. Give honest and sincere appreciation.
1. Arouse in the other person an eager want.

### Six ways to make people like you

1. Become genuinely interested in other people.
1. Smile.
1. Remember that a person's name is to that person the sweetest and most important 
sound in any language.
1. Be a good listener. Encourage others to talk about themselves. 
1. Talk in terms of the other persons interests.
1. Make the other person feel important and do it sincerely.

I familiarize myself with these rules anytime I'm going to meet people.  It helps me 
condition my thinking into that of the other person.  I want to leave the person with 
a positive experience of me and the company I represent.  

## Talk with the sponsors

Conference sponsors are the life blood of any conference. Their monetary contributions 
are what allow organizers to do the extras. In exchange, they're hoping for a moment of 
your time. They'll even give you swag. 

Spend some time in the exhibition booth. Use the network skills above to talk with the 
sponsors. You may find a service or product that interests you. Have your business cards 
with you to hand out. Think of sponsors as eager networkers who jumped the line to talk 
with you. 

## Enjoy yourself

Remember that attending a conference is more then sitting in sessions. The real value is 
talking with others and building relationships. AtlasCamp 2015 is coming up fast and is 
the perfect event to get the most out of with these tips. Make sure to talk with me at 
AtlasCamp. You can find me [@RedWolves](http://twitter.com/redwolves) on Twitter.

### Photo Credits

* [woidda - AtlasCamp Europe 2012](https://www.flickr.com/photos/woiddawoidda/7010438419/)

