---
title: "So you want to speak at a conference..."
date: "2015-04-30T16:00:00+07:00"
author: "jgarcia"
categories: ["Presenting", "Git Merge"]
---

Speaking at a conference is an important opportunity! Getting selected for a
session, then writing and delivering a compelling presentation, can be a
daunting and nerve-wracking task. I've collected my insights to help rookie
Advocates understand the process of securing an opportunity and delivering a
quality talk. Presentations at conferences are a key chance to build your
portfolio as a developer, get review for your research or product ideas, foment
excitement for a new product or service offering, and have a great
time meeting other technical folks. Read on for insights into my
experience as a speaker at this year's [Git Merge](http://git-merge.com) in
Paris.

# Lead up: Getting in and getting prepared
## Leveraging our contacts

<blockquote class="twitter-tweet tw-align-center" data-partner="tweetdeck"><p><a href="https://twitter.com/hashtag/Atlassian?src=hash">#Atlassian</a> loves git. <a href="https://twitter.com/hashtag/gitmerge?src=hash">#gitmerge</a> <a href="http://t.co/EYdrcVGiR0">pic.twitter.com/EYdrcVGiR0</a></p>&mdash; Twidi (@twidi) <a href="https://twitter.com/twidi/status/586171315595452417">April 9, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

It all started with [Nicola](https://twitter.com/durdn) spotting a thread about
a potential 10th Anniversary party in the Git mailing list.
[Grace](https://twitter.com/gracefr), the head of my department, initiated
discussions with the contributor and the [Linux
Foundation](http://www.linuxfoundation.org) to brainstorm about the best way to
celebrate. Through these discussions and the Linux Foundation, we were able to
get together with the Git Merge organizer over a friendly lunch so we could sell
him on the value of a presentation from our team. We identified a topic of
general interest to the Git community that Atlassian had done recent research
into, and agreed to submit a proposal.

###### Insights:

- Follow mailing lists and Twitter feeds for the projects you care about, so you
can find out about opportunities early.
- Your network is crucial. If you don't know people yet, get to know them by
contributing to projects and mailing lists, or attending conventions whenever
 you can.
- Often, your leaders will have great contacts and will have an easier time
getting access to the right people.
- Take opportunities as you get them, especially as you're getting started. All
exposure is valuable.
- Don't panic. The first session you do will be, by definition, the most
 important one you've ever done.

## Writing the proposal and presentation

Writing the proposal was straightforward; a few hours of research on the
internal project and a bit of word-craft to make it sound exciting were applied
to the task. I wrote a two paragraph summary of the idea, and my team (h/t
[Tim](https://twitter.com/kannonboy)) helped fill it out into a proper pitch. We
submitted as soon as we could turn it around. I found out that a winning
submission will have several hundred words that make the value of your talk as
clear as possible: the problem you intend to solve, the value of your solution
to the audience, and the overall message of your talk.

Once we had been approved for a session, I set out to build a presentation.
Researching the topics, I built an outline in
[Confluence](https://www.atlassian.com/software/confluence) to store my ideas
and keep them organized, and then created my Keynote deck based on that page. I
found out later in the review stage that I had organized the presentation with
an _exposition_ frame rather than a _story_ frame; this caused some significant
re-work which could have been prevented by paying closer attention to the
overall goal of the talk. I spent a fair amount of time recording a demo video,
which turned out to be a great way to show off the project while being able to
talk coherently; this also eliminated the chance that my equipment, or the
equipment at the convention hall, would fail and leave me stranded.

###### Insights:

- Writing a great pitch is the most important part of getting a chance to go on
stage.
- Be sure to sell your talk. Explain how it relates to the convention, what
problem you're addressing, and be clear with your overall message.
- Find the right tone early. Determine if you're telling a story, presenting an
argument for something, or calling to action.
- Use as few words on the slides as possible. The audience should be listening
 to you and not reading the screen.
- It's best to record the demo beforehand so you will be free to provide
 narration. This precludes technical difficulties.
- Keep animations simple. Re-use a small array of animations (perhaps 1-3) to
 avoid distracting your audience; don't use a wide variety.
- Make sure you have lots of review. Your team lead, team mates, and other
co-workers will have a wealth of insight.

## Practice, practice, practice!

I was fortunate to be able to arrange several hours of public speaking training
from one of San Francisco's premier voice coaches, which was a critical part of
our strategy to present a compelling session in Paris. In addition to this time,
I logged upwards of 40 hours practicing the presentation in front of team mates,
co-workers, other teams, developers, stakeholders, and even a tech writer and a
data scientist for good measure. I even practiced in front of my
technically-inclined wife, my non-technical personal friends, and a mirror.

###### Insights:

- Get coaching if you have budget for it. It's worth every dime.
- Practice as much as you can! You could practice up to six hours for each
minute of session.
- Get as many fresh eyes as you can for each practice session. A variety of
technical and non-technical reviewers can help prevent you from being boring.
- Make sure that people who are similar to your audience are able to understand
your overall message.

## Feedback, feedback, feedback!

Because I didn't initially have a very strong command of the subject matter, I
needed a lot of feedback to get the presentation right! The best way to make
sure I get reliable feedback, in my opinion, is to invite as many people as I
can, with as diverse of a skill set as I can find. For me, the most valuable
input beyond that of the extensive Advocacy experience of Grace came from
[Alison](https://twitter.com/alihuselid), the Head of Product Marketing and our
Technical Writer, Dan. The method you use to take in feedback and synthesize it
into your deck is your own to choose; however, I assure you that when it comes
to reviewers, more is definitely better. Working with so many reviewers helped
me to become more confident and comfortable with the subject matter.

###### Insights:

- Find a diverse set of reviewers. Different viewpoints will make a better
presentation!
- Invite someone with a background in technical writing or information design to
review, if possible. They'll have essential insights.
- Marketing or Demand Generation team members can help find the best choice of
words and tone.
- Be sure that your Legal department has a clear view of the contents to avoid
any unforeseen outcomes.
- Don't sit on feedback! Fold suggestions into the deck while they're fresh in
your mind.
- Know your material! Nothing will carry you through difficult times as well as
a solid understanding of the information you've come to present.

# Taking the plunge
## The first day

<blockquote class="twitter-tweet tw-align-center" data-partner="tweetdeck"><p>Once is an accident. Twice is coincidence... <a href="https://twitter.com/bitbucketeer">@bitbucketeer</a>&#39;s talk was the 4th one at <a href="https://twitter.com/hashtag/gitmerge?src=hash">#gitmerge</a> about git troubles with large repos/files.</p>&mdash; Vadim Zeitlin (@_VZ) <a href="https://twitter.com/_VZ/status/586177314670665728">April 9, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Details about the first day of the event can be found at
Nicola's excellent synopsis [Notes from Git
Contributors'
Summit](https://developer.atlassian.com/blog/2015/04/git-merge-2015-wrap/). The
Git Merge organizers had a full agenda for the day, including a Walking Tour of
Paris and a Speakers' Dinner where the lineup of presenters were afforded a
chance to get to know one another over wine. We had some great discussion,
especially because so many of our talks had such similar topics. Crucially, this
meeting gave me a chance to compare notes with a presenter who would be giving a
very similar talk. This helped us avoid confusing the audience because we took
the time to synchronize our messages. I brought a rather unfortunate head cold
with me from San Francisco that had me sniffling and coughing throughout the
plane ride and my time in Paris. Apologies to anyone who I transmitted it to!

###### Insights:

- Get to the host city early. A day to see the city and get to know the
atmosphere will do wonders.
- Meet the other presenters beforehand, if possible. This will give valuable
insight into the experience the audience will have during the convention.
- Talk about presenters with similar topics, to avoid surprises. Don't force
attendees to hash out conflicts between presentations.
- Enjoy day-before festivities as a chance to unwind.

## Last minute preparation

On the night before my talk, it's critical that I get adequate sleep to
function. I like to get just as much as I can manage, up to eight hours; this
helps me stay alert and avoid excess consumption of caffeine on the morning of
the event. In my case, the jetlag bug bit and I got a paltry three hours of
sleep. That morning, I had a bit of coffee but then switched to orange juice for
a more natural alertness.  First thing after arriving at the convention and
debugging the Null Coffee Exception in my brain, I made a point to run around
and get all of the essential details of how and when I would be expected to go
on stage, to ensure there was no confusion when I was called. I had a few last
minute edits to make - there's always last minute edits - and I made sure to
practice the whole talk with Nicola and [Steve](https://twitter.com/tarkasteve),
who were in town. To get this done, we retreated to Nicola's hotel room and
rehearsed everything during the lunch break.

###### Insights:

- Be sure to secure a quiet place to make any last minute edits.
- Practice your presentation at least once on the day of the event. Don't be
afraid to miss other sessions to do this.
- Be cautious of too much coffee - caffeine jitters are no fun on stage! Try a
juice that's high in Vitamin C for natural alertness.
- Get details about when and where you're expected to show up well in
advance.
- Watch as many sessions as you can to get a feel for the audience.
- Pay attention to the type of mic the venue is using in case you need to make
any adjustments.

## Going on stage

<blockquote class="twitter-tweet tw-align-center" data-partner="tweetdeck"><p><a href="https://twitter.com/bitbucketeer">@bitbucketeer</a> is tearing it up at <a href="https://twitter.com/hashtag/gitmerge?src=hash">#gitmerge</a> <a href="http://t.co/Jsa7KvepAb">pic.twitter.com/Jsa7KvepAb</a></p>&mdash; Steve Smith (@tarkasteve) <a href="https://twitter.com/tarkasteve/status/586174167453667330">April 9, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Thankfully, the organizers provided the crowd with a fifteen minute recess
between talks, and this allowed time for me and the mic to become friendly with
each other. It also gave me some time to test out the stage, which is a great
way to get comfortable with speaking on it. I walked the length and breadth of
the stage, testing out my remote control from all four corners. Confident it
would work reliably throughout the presentation and that I had a good
understanding of the space, I waited to be called up by [Scott
Chacon](https://twitter.com/chacon), the master of ceremonies for the event.
Notably, my sniffles and stuffy nose symptoms cleared up as my excitement to be
on stage grew; this is a normal reaction to that type of stress and one of the
many benefits to it.

###### Insights:

- Walk the stage and test your equipment before presenting if possible.
- Show up at least five minutes before your scheduled time.
- Be sure to make any needed bio breaks before your time comes up.
- Don't panic. You're going to be nervous, this is normal and natural.

## Presenting and aftermath

Once I was called, I made my presentation. Even though I was nervous, the hours
of preparation really paid off - it was a great talk! I presented the results of
our research clearly and competently, and kept most everyone entertained the
whole time. The [resulting
Tweets](https://twitter.com/bitbucketeer/timelines/588185381809577985) were very
appreciative! I scanned the crowd while on stage, and while there were a few
drowsy heads out of the three hundred or so developers in attendance, I felt
like the crowd was engaged and excited to hear my story! After the talk, there
were some great, encouraging words from many attendees as well as some very
intriguing questions. I'll admit I did forget to read some of the anecdotes I
had prepared, but that's okay.

###### Insights:

- Don't worry about it. The presentation the audience is there to see is the one
you're there to give; therefore, the crowd is very forgiving.
- No matter how good or bad you are, the law of averages demands that some
attendees will love and some will not love your presentation. Accept it.
- If you forget a part of the speech, don't say so on stage, because nobody will
know. Either omit it completely or find a way to inconspicuously add it in
later.

#Afterparty!
## Celebrate a job well done!

<blockquote class="twitter-tweet tw-align-center" data-partner="tweetdeck"><p>Great talk at <a href="https://twitter.com/hashtag/gitmerge?src=hash">#gitmerge</a> from the awesome <a href="https://twitter.com/bitbucketeer">@bitbucketeer</a> John Garcia <a href="http://t.co/PWCYa84jNX">pic.twitter.com/PWCYa84jNX</a></p>&mdash; Dirk Lehmann (@doergn) <a href="https://twitter.com/doergn/status/586176134171271168">April 9, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Our hosts threw an amazing afterparty at a former crate factory, with amazing
food and beverage on hand. I had a great time talking with guests and our hosts,
swapping stories, and generally having a grand time. I had a bit of star power,
and that makes for a fun party! Reports from my team in San Francisco were
enthusiastic, so I'm happy to say it was a great result!

A huge thank-you to [GitHub](https://github.com) for presiding over a most
excellent convention and birthday party!

###### Insights:

- Talk to the other presenters and attendees after the event to share feedback.
- Be sure to give feedback to other presenters, as well!
- This is a great time to make professional contacts. Don't miss out!
- Have a nice cup of coffee and celebrate a job well done.

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
