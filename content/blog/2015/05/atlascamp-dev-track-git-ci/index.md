---
title: "Git with the Program and sign up for AtlasCamp"
date: "2015-05-14T15:00:00+07:00"
author: "gfrancisco"
categories: ["git", "ci", "atlascamp"]
---
<style>
  .right-image {
    float: right;
	margin: 1em;
  }
  .left-image {
    float: left;
    clear: both;
	margin: 1em;
  }
</style>

[AtlasCamp](https://www.atlassian.com/atlascamp#track2) is right around the
corner on June 9-11 in Prague, and will be like no other AtlasCamp. For the
first time we have two tracks of content, starting with our usual track focused
on how to customize, integrate with, and extend our products. This year we've
also introduced a brand new track focused on sharing great developer content to
help you build great software and services.

<img class="right-image" style="width: 300px;" alt="AtlasCamp" src="https://www.atlassian.com/wac/company/about/events/atlascamp/2015/sectionWrap/0/column/0/imageBinary/atlascamp-logo.svg"/>

Our main conference still has some room for new registrants but you'll need to
[sign up](https://www.eventbrite.com/e/atlascamp-2015-tickets-15898089614?aff=referral)
soon as we're getting close to capacity.

## Design, code, build and deploy more efficiently and effectively

[The new track](https://www.atlassian.com/atlascamp#track2) has educational
sessions on Agile, CI and Git. We're sharing our learnings building SaaS
solutions and providing un-interrupted service to our customers at scale. You'll
learn about technologies like Docker, React, JSX, and Flux. Speaking of builds -
learn how to manage the complexities of CI/CD from our own war stories doing this
across our different products and services.

Some highlighted talks you won't want to miss:
* Our own [Sven Peters](https://twitter.com/svenpeters) - Java developer and
evangelist - will be delivering his ever popular Coding Culture talk
* [Emma Jane Westby](http://emmajane.net/) - a Git trainer and author on
"Reverse engineering people"
* [Holly Cummings](https://twitter.com/holly_cummins) - Sr. Software Engineer
at IBM shares "Confessions of an automation addict"
* [Greg Warden](https://twitter.com/usmile1) - Atlassian Engineering Services
leader who will talk about how the successes of SaaS companies like Facebook
has shaped their mission in his session "Damn you Facebook"
* Principal Engineer,
[Erik van Zijst](https://twitter.com/erikvanzijst) will be taking you into the
depths of the inner guts of Bitbucket which serves 3M developers and growing.


If you want to do some deep dive training, we have a few spots left in our
workshops being delivered by our resident devops expert [Steve Smith](https://twitter.com/tarkasteve)
who's teaching a Docker + Bamboo workshop and Git expert [Nicola Paolucci](https://twitter.com/durdn)
who's delivering a training on Advanced Git - "Master the art and practice of
DVCS".

Beyond all these great sessions, we'll have developer breakouts (mini tech
talks), network time for you to chat with speakers, other attendees, and we'll
even have technical folks from AWS, Azure, and Google there to answer your cloud
questions.

I'll be MC'ing this track and hope to see you there. Let [me](https://twitter.com/gracefr)
know if you're coming and particularly if you are a Woman in Tech I've got a
special networking event for you at AtlasCamp I'll be happy to send you an
invite. See ya in Prague!

<div class="signup-container"><a class="aui-button aui-button-primary" href="http://atlascamp.com">Register for AtlasCamp</a></div>
