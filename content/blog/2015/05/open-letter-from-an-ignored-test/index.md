---
title: "Open letter from an @ignored test"
date: "2015-05-12T14:00:00+07:00"
author: "medo"
categories: ["testing", "qa"]
---

<style type="text/css">
.lede {
  display: none;
  visibility: hidden;
  font-style: italic;
}
.blog-archive .lede {
  display: block;
  visibility: visible;
  font-style: normal;
}
</style>

<p class="lede">We've all done it: a perfectly good test goes flakey, and 
instead of rolling up our sleeves right then and there, we slap an @ignore 
annotation on it. But do we ever take the test's feelings into consideration? 
No. No, we do not. Here's one @ignored test who thinks it's high time we do.</p>

Dear developer,

I've been wanting to talk to you for a while now, but words don't always come 
easy. We've had some really fun times together. I still remember the first time I 
warned you about a minor bug in your code, and how happy you were for having 
me in your life! Do you remember it? I also remember the first time you 
refactored me to make me more efficient and how well-written I felt afterwards... 
ah, great times! 

I owe you everything, I know. And I'm thankful for it. I wouldn't
exist if it weren't for you. You thought that I was needed so you created me, 
and from that moment on I am at your service, and I am glad to be, as you gave 
me a purpose. **I want to catch bugs for you.** I want to give you assurance that 
things will continue to work after your changes. I want to make your life easier, 
and you know I can do all those things, I know you do.

But then, with no clear explanation, I started to fail sometimes, for no 
specific reason. Something broke a little inside of me. I was able to continue
functioning almost normally, but I couldn't avoid causing red builds from time to 
time, it was simply out of my control. **I became... _flakey_**. My flakiness upset
you, and I am not angry about that, as it upset me too. I was not reliable anymore.
I lost my purpose. At this point, I have to say, it hurts me to remember how 
you reacted after some weeks of flakiness: instead of investing some love 
and dedicate a couple of hours to fix me and get me back to a good state, 
**you annotated me as _@ignore_ and abandoned me** in an immense and desolate codebase.

My statements and assertions can't help to shed a tear when I think of this. 
For an automated test, being flakey is bad - but at least I passed successfully 
from time to time, and my failures were a reminder that I needed some of your 
magic; but being ignored my friend... that is simply terrible. If there is a hell
for automated tests, it definitely is being annotated as _@ignore_ and forgotten, 
being surrounded by successful tests that go green and not being able to join 
them, watching builds pass by and not pick me up, sitting between infinite 
lines of code, hopelessly waiting, needing a fix and not being taken care of... 
I would never wish that even to my worst automated test enemies.

Don't get me wrong, I understand that automated tests have a lifecycle, and
eventually they get replaced by other automated tests, better and more modern. 
Sometimes our flakiness can't be resolved, so we need to be removed or replaced, 
and that's ok. Sometimes the code we are testing is simply retired, so we have no 
purpose anymore, and that's ok as well. It's part of who we are. But hey: I am 
code too, you know? I need attention! I need to be implemented and refactored
properly to achieve my purpose! I need code reviews where you look at me with care 
and spot issues that I might have, because **tests can have bugs too!** It's 
simply unfair to only look after feature code and, when forgotten tests start to 
fail, annotate them as _@ignore_ and continue your day as if nothing happened. It's outrageous!

All I am asking is for you to make up your mind about me, **either fix me or 
delete me, but do not forget about me!** You break my statements when you do. 
Humans have issues with decisions, as we lines of code know, so if you
need to get away with a green build and ignore me for a couple of runs, it's
fine. Really! But if you are not going to come back immediately and find 
what's wrong with me and why have I been flakey recently, have some decency 
at least: raise an issue in your bug tracker, so that someone else can give me
the attention I need to get back on track and provide some value again. It's
not that hard, is it? Please? For all the green builds we've had together?

I sincerely hope we can sort out our differences soon.

Forever yours,

![@Ignored Test](signature.png "@Ignored Test")
