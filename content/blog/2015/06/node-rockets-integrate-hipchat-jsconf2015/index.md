---
title: "Node Rockets integrated with HipChat flew over JSConf 2015"
date: "2015-06-09T15:00:00+07:00"
author: "rwhitbeck"
categories: ["HipChat", "Node.js", "atlassian-connect"]
---

On May 27th, JavaScript developers descended onto Amelia Island, Florida for 
[JSConf 2015](http://2015.jsconf.us/). Atlassian again, sponsored this years 
[Node Rockets](http://www.noderockets.com/) hack day event.

Sandwiched between two days of amazing talks from the top JavaScript developers 
is a day of hacking. Again this year the Node Rockets team put together a day of 
building and programming sponsored by Atlassian.  

Building a rocket involved wiring sensors to the Raspberry Pi. Custom coding the 
telemetry data with Node.js. Cutting and assembling the soda bottles to house the 
electronics. And a bottle that houses the pressurized water that makes the rocket 
fly.   
 
Putting a rocket together took most of the day with assembly and QA testing.  As 
this was an Atlassian sponsored activity we wanted to add a little extra incentive. 
We offered Amazon gift cards to the team that had the best integration with 
[HipChat](https://www.hipchat.com/docs/apiv2) using 
[Atlassian Connect](http://connect.atlassian.com). 

Many teams put together a good effort and we're happy with what the winners came up 
with. You can see more from that day as well as an interview with the winning team 
in the video below. The Atlassian's team rocket launched their rocket five times 
that day. 
<div style="margin-top: 30px; text-align: center;">
<iframe width="853" height="480" src="https://www.youtube.com/embed/Epwmv9o7qeY" frameborder="0" allowfullscreen></iframe>
</div>