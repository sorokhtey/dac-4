---
title: "Tip of the Week: To fail to plan is to plan to fail"
date: "2015-06-15T16:00:00+07:00"
author: "jgarcia"
categories: ["totw","Bamboo"]
---

This week's tip also comes to us from [Wittified's](http://www.wittified.com)
[Daniel Wester](https://twitter.com/dwester42a), who shares his strategies for
trouble-free Bamboo test plan creation.

## Plan out your build plan

> A build plan should be repeatable in order to help other developers (and
yourself) to debug any build issues.

\- Daniel Wester

Before setting your work plan in
[Bamboo](https://www.atlassian.com/software/bamboo),  spend some time
considering what you'd like it to do specifically. This means talking to the
team to discover or set shared expectations and ensure repeatability of the
plan. At Atlassian, we use
[Confluence](https://www.atlassian.com/software/confluence) frequently to
discuss and diagram our build plans.

## Build plans should work from the command line

> if you can script the build plan on the command line it will let you easily
run it locally

\- Daniel Wester

Builds that work from the command line speed prototyping and testing by allowing
the developer to close the loop through local testing. This speeds up the
debugging process as well. While there are certain "magical" features in Bamboo,
if you can give your developers functioning test scripts you can prepare them to
spot their own bugs before Bamboo does.

Let us know what you think in the comments!

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
