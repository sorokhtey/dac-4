---
title: "Git power routines [video course]"
date: "2015-07-22T06:00:00+07:00"
author: "npaolucci"
categories: ["git","video"]
---

Follow along or just sit back and enjoy a live, hands on tutorial on the power
routines of experienced git users. We'll explore with real world examples how
to amend commits, do an interactive rebase - and why would you want to do one
in the first place, how to solve conflicts without any merge tools, the power
of less known merge strategies, how to do interactive commits, and much more.

<iframe width="850" height="500" src="https://www.youtube.com/embed/shMQ5xHENAM?list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_" frameborder="0" style="margin: 15px 0 10px 0" allowfullscreen></iframe>

## Course notes

### Part 1/8: Introduction

- Who am I?
- Content overview.
- Choice of test project.

### Part 2/8: Housekeeping

- [Find my aliases here].
- Enhance your shell with [liquidprompt].
- Start with an empty commit.
- Short cuts to list commits with nice annotations:

```
git log --decorate --oneline
git log --decorate --numstat
```

### Part 3/8: Amending and rebasing

- Amend a commit.
- Reset a commit to perform a rename.
- Different resets affect different parts of a git repository.

### Part 4/8: Interactive rebase

How to perform an interactive rebase to remove a binary file stuck in the
repository

### Part 5/8: Solving conflicts

In this 5th part of the course we'll show a few concepts useful when solving
merge and rebase conflicts with an interactive example on how to solve one.

We'll explain `--ours`, `--theirs`, conflict markers and what's the process
needed to solve a conflict using Git.

### Part 6/8: What is a merge and alternative merge strategies

We cover the basics of what a merge is in Git and we show the use of a couple
of less known merge strategies like the "ours" merge strategy and the "octopus"
strategy.

### Part 7/8: Git interactive add

In this part we show how to perform and interactive add using Git. That is
splitting the contents of some change across to more semantically meaningful
commits.

### Part 8/8: How to use Git stash

How to use git stash to solve common workflow situations, swap context with
ease and smoothness and look cool to your colleagues.

## Slides of the course

<iframe src="//www.slideshare.net/slideshow/embed_code/key/jB5mZGkUC5k5q" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/durdn/git-power-routines" title="Git Power Routines" target="_blank">Git Power Routines</a> </strong> from <strong><a href="//www.slideshare.net/durdn" target="_blank">Nicola Paolucci</a></strong> </div>

## Conclusions

I hope you enjoyed this material and if you have any questions feel free to ask
here or ping me at [@durdn] or my entire team at [@atlassiandev].

*Part of the footage of the session was recorded at [AtlasCamp].*

[part 1]: https://www.youtube.com/watch?v=shMQ5xHENAM&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_&index=1
[part 2]: https://www.youtube.com/watch?v=xxIUW4BdASw&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_&index=2
[part 3]: https://www.youtube.com/watch?v=JLf4obVDz-s&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_&index=3
[part 4]: https://www.youtube.com/watch?v=3GVaxUDF3Q4&index=4&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_
[part 5]: https://www.youtube.com/watch?v=4dJcsF6hgS4&index=5&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_
[part 6]: https://www.youtube.com/watch?v=78erWDZboGs&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_&index=6
[part 7]: https://www.youtube.com/watch?v=gIGRexPQ-PE&index=7&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_
[part 8]: https://www.youtube.com/watch?v=ZlLHUok4RDs&index=8&list=PLDshL1Z581YYxLsjYwM25HkIYrymXb7H_
[@atlassiandev]: https://twitter.com/atlassiandev
[@durdn]: https://twitter.com/durdn
[AtlasCamp]: https://www.atlassian.com/atlascamp
[liquidprompt]: https://github.com/nojhan/liquidprompt
[Find my aliases here]: https://bitbucket.org/durdn/cfg/src/master/.gitconfig
