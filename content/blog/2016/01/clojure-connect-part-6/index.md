---
title: "Let's build a Bitbucket add-on in Clojure! - Part 6: Finally, ClojureScript!"
date: "2016-01-28T06:00:00+07:00"
author: "ssmith"
description: "This installment introduces using ClojureScript to talk to integrate with Atlassian Connect and talk to Bitbucket."
categories: ["bitbucket", "atlassian-connect", "clojure", "clojurescript", "javascript", "bbclojureseries"]
---

<style>
  .float-image {
      display: block;
      margin: 15px auto 30px auto;
  }
  .shadow-image {
      display: block;
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px 5px #888888;
  }
</style>


Back in [part 4][part4] of this series we introduced two methods of accessing
Bitbucket from our Atlassian Connect add-on, including via the client-side
(browser) [JavaScript API][bitbucket-js]. However as we've written all of our
server-side code in Clojure so far, it's a shame to have switch to another
language. In this installment we'll take a look at [ClojureScript] and how we
can integrate it into our project.

## Revisiting our Connect JavaScript

In [part 4][part4] we introduced accessing the Bitbucket API via the JavaScript API.
This API uses the [cross-domain messaging][xdm] standard to open a communication
channel between the parent (i.e. Bitbucket) page and the iFrame child page (i.e.
embedded Connect add-on content). This API uses a special `AP.require()` call to
have the parent page make asynchronous calls on behalf of the child iFrame. We
provided some examples of using this API in part 4 of of this series:

```javascript
// Bitbucket Connect also supports a client side library - AP (for "Atlassian Plugins") - that
// allows you to interact with the host application. For example, you can make authenticated
// requests to the Bitbucket REST API ...

AP.require('request', function(request) {
    request({
        url: '/1.0/user/',
        success: function(data) {
            $('#displayName')
                .text(data.user.display_name)
                .next('.loading').hide();
        }
    });
});

// ... and set cookies (browser security policies often prevent this from being done in iframes).

var COOKIE_NAME = 'example-visits';

AP.require('cookie', function(cookie) {
    cookie.read(COOKIE_NAME, function(visits) {
        visits = (visits ? parseInt(visits) : 0) + 1;
        cookie.save(COOKIE_NAME, visits, 30);
        $('#pageVisits')
            .text(visits)
            .next('.loading').hide();
    });
});
```

However as we've written our add-on in pure Clojure so far it seems a shame not
go the whole way. Enter [ClojureScript]...

## So what is ClojureScript?

[ClojureScript] is a version of Clojure that targets JavaScript and
[Google Closure][Closure]. This allows developers to write a major subset of
Clojure that will run in the browser. This brings
[a number of advantages][cljs-rationale] such as
[immutable/persistent data structures][clj-peristent], [macros], and access to
channel-based concurrency via [core.async] (the benefits of which we'll get into
in a later installment). So let's port our JavaScript which interfaces with the
Bitbucket API to ClojureScript...

## Restructuring our project

But the first thing we'll want to do is restructure our project a little. By
default Leiningen places all Clojure code into `src/`. However we'll now be
working with two compilers (one for Clojure and another for ClojureScript), so
it's good practice to separate the two codebases, so we'll move them into
separate subdirectories:

``` shell
[ssmith:~/projects/hello-connect] $ mkdir -p src/clojure/
[ssmith:~/projects/hello-connect] $ mv src/hello_connect/ src/clojure/
[ssmith:~/projects/hello-connect] $ mkdir -p src/cljs/hello_connect/
```

Now we have `src/clojure` and `src/cljs`; we just need to tell Leiningen about
this change in our `project.clj`:

``` clojure
:source-paths ["src" "src/clojure"]
:test-paths   ["test" "test/clojure"]
```

## Translating from JS to CLJS

Now that we have a home for our ClojureScript module, let's produce a straight
port of [Tim Pettersen's][tpettersen] [example JavaScript][connect-intro]:

``` clojure
;; Bitbucket Connect also supports a client side library - AP (for "Atlassian Plugins") - that
;; allows you to interact with the host application. For example, you can make authenticated
;; requests to the Bitbucket REST API ...

(ns hello_connect.core
  (:require [cljs.reader :as reader]
            [goog.dom :as dom]
            [goog.style :as style]))

(enable-console-print!)

(defn set-name [data]
  (let [name (((js->clj data) "user") "display_name")]

    (-> (dom/getElement "displayName")
        (dom/setTextContent name))
    (-> (dom/getElement "nloading")
        (style/setElementShown false))))

(.require js/AP "request"
          (fn [request]
            (request (clj->js
                      {"url" "/1.0/user/"
                       "success" set-name}))))

;; ... and set cookies (browser security policies often prevent this from being done in iframes).

(def cookie-name "example-visits")

(defn set-count [count]
  (-> (dom/getElement "pageVisits")
      (dom/setTextContent count))
  (-> (dom/getElement "cloading")
      (style/setElementShown false)))

(.require js/AP "cookie"
          (fn [cookie]
            (.read cookie cookie-name (fn [visits]
                                        (let [n (inc (reader/read-string visits))]
                                          (.save cookie cookie-name n 30)
                                          (set-count n))))))
```

As you can see above, this looks like a mixture of Clojure and JavaScript. The
first thing we do is import some of the Closure and Clojure utilities, and then
define some functions to use these libraries to manipulate the DOM and set our
values. Then we make two calls to the [Bitbucket AP.require() API][bitbucket-js]
with callbacks to lookup the values. Which is to say our code functions almost
exactly the same as the original. The only major difference is that we're using
Google's [Closure] library to manipulate the DOM instead of jQuery.

## Building ClojureScript

We put this code into `src/cljs/hello_connect/core.cljs`, and now we need to
compile it. Naturally Leiningen [has a plugin][cljsbuild] to help with this, so
let's add it to the `:plugins` section:

``` clojure
[lein-cljsbuild "1.1.1"]
```

We also need to add ClojureScript itself to the `:dependencies` section:

``` clojure
[org.clojure/clojurescript "1.7.48"]
```

(As always you can use [lein ancient] to keep these dependencies up to date.)

Now we need to configure the compiler itself, which has some trade-offs and may
require some explanation...

## Understanding the Clojure/Closure compiler

The [Google Closure][closure] compiler has a number of optimisation levels that
have different effects on the code-base. The main two we're interested in are
`:none`, which is recommended for development, and `:advanced` which is
recommended for production. However `:none` requires the developer to provide
some dependencies manually which are not required in production (as `:advanced`
does more packing and minimisation); this effectively means you must use
slightly different HTML in dev, which is not a great idea. It is usually
considered bad practice to make a lot of changes moving from dev to production.
On the other hand the `:advanced` optimisations are _really_ slow (11 second
compilation time for our simple ClojureScript code). However like
Clojure/Leiningen's slow start-up time not being an issue in practice because we
always leave the REPL running, in practice we don't rebuild ClojureScript from
scratch. While ClojureScript supports REPL development, another option is to use
its incremental compilation. Basically all we need to do is run `lein cljsbuild
auto` and not only will it compile the code but it will watch the source files
and initiate a conditional compilation on change, which is much faster:

``` shell
[ssmith:~/projects/hello-connect] $ lein cljsbuild auto
Watching for changes before compiling ClojureScript...
Compiling "target/cljs/public/cljs/core.js" from ["src/cljs"]...
Successfully compiled "target/cljs/public/cljs/core.js" in 10.46 seconds.
Compiling "target/cljs/public/cljs/core.js" from ["src/cljs"]...
Successfully compiled "target/cljs/public/cljs/core.js" in 2.489 seconds.
```

And for more advanced development scenarios there is also [Figwheel], which will
also live-load ClojureScript into a running browser window.

So we want to build our ClojureScript with `:advanced` optimisations, however
there is one gotcha in doing that. `:advanced` is very aggressive and will
mangle any function names, including functions that are external to our code; in
this case the `AP.require()`/`(.require js/AP)` calls. There a couple of ways to
fix this, but the least intrusive is to just declare any 'protected' symbols to
the compiler. This is done by adding them to a file on the path, in our case
under `resources/AP-externs.js`:

``` javascript
var AP = function() {}
AP.require = function(method, func) {}
```

This declares that the `AP` module and `require()` functions should not be
mangled.

## Building ClojureScript (again)

Now we can give Leiningen the information it needs to compile our code in our
`project.clj`:

``` clojure
:cljsbuild {:builds
            [{:source-paths ["src/cljs"]
              :compiler {:output-to "target/cljs/public/cljs/core.js"
                         :externs ["AP-externs.js"]
                         :optimizations :advanced}}]}
```
                         
This compiles our code into a single file `core.js` if we issue `lein cljsbuild
auto`. We'd also like to include this code when building our `uberjar`, so we
add the output directory into the resources path so it gets merged with all our
other HTML, CSS, etc:

``` clojure
:resource-paths ["resources"
                 "target/cljs"]
```

Of course, we also need to update our HTML to use our generated JavaScript. Open
our template `connect-example.selmer` and change the line:

``` html
<script src="/js/addon.js"></script>
```
    
to:

``` html
<script src="/cljs/core.js"></script>
```

The last thing we need to do is tell Leiningen that we would like our
`cljsbuild` called as part of any builds it performs:

``` clojure
:hooks [leiningen.cljsbuild]
```

And that's it. We can now build and run our uberjar as we did in part 5, and we
can see our ClojureScript running in the browsers.

### Next time

We've produced a lot of boilerplate code over these 6 parts of our tutorial.
But boilerplate isn't something that fits into the Clojure world; if something
is repetitive then it should be automated away. In the next installments we're
going to have a look at some of the ways Clojure and its ecosystem enables this.

### The code

The code for this part of the tutorial series is available in
[the `part-6` tag][part6-code] in the
[accompanying `hello-connect` Bitbucket repository][hello-connect]. There will
also be code appearing there for the later parts as I work on them if you want
to skip ahead.


[bb-connect-announce]: https://blog.bitbucket.org/2015/06/10/atlassian-connect-for-bitbucket-a-new-way-to-extend-your-workflow-in-the-cloud/
[connect-intro]: https://developer.atlassian.com/bitbucket/guides/introduction.html
[JWT]: https://en.wikipedia.org/wiki/JSON_Web_Token
[JWT-qsh]: https://developer.atlassian.com/bitbucket/concepts/understanding-jwt.html
[clj-connect]: https://bitbucket.org/ssmith/clj-connect
[Leiningen]: http://leiningen.org/
[Boot]: http://boot-clj.com/
[Maven]: https://maven.apache.org/
[JDK]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Luminus]: http://www.luminusweb.net/
[composable]: https://en.wikipedia.org/wiki/Composability
[lein ancient]: https://github.com/xsc/lein-ancient
[Compojure]: https://github.com/weavejester/compojure
[Ring]: https://github.com/ring-clojure/ring
[Swagger]: http://swagger.io
[12 Factor]: http://12factor.net/
[Jetty]: http://www.eclipse.org/jetty/
[Immutant]: http://immutant.org/
[Undertow]: http://undertow.io/
[s-expressions]: https://en.wikipedia.org/wiki/S-expression
[Handlebars]: http://handlebarsjs.com/
[ERB]: http://apidock.com/ruby/ERB
[Selmer]: https://github.com/yogthos/Selmer
[Django]: https://www.djangoproject.com/
[ngrok]: https://ngrok.com/
[bb-start]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[lein-profiles]: https://github.com/technomancy/leiningen/blob/master/doc/PROFILES.md
[storage.clj]: https://bitbucket.org/ssmith/bitbucket-docker-connect/src/HEAD/src/clojure/docker_connect/storage.clj?at=master
[EDN]: https://github.com/edn-format/edn
[clojure-json]: https://github.com/clojure/data.json
[ring-json]: https://github.com/ring-clojure/ring-json
[clj-jwt]: https://github.com/liquidz/clj-jwt
[bb-docker-connect]: https://bitbucket.org/ssmith/bitbucket-docker-connect
[Bitbucket]: https://bitbucket.org/
[Clojure]: http://clojure.org/
[docker-hub-announce]: https://developer.atlassian.com/blog/2015/09/docker-bitbucket/
[tpettersen]: https://developer.atlassian.com/blog/authors/tpettersen/
[bb-node-example]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
[Cider]: https://github.com/clojure-emacs/cider
[Fireplace]: https://github.com/tpope/vim-fireplace
[La Clojure]: https://plugins.jetbrains.com/plugin/4050
[Cursive]: https://cursiveclojure.com/
[descriptor-doc]: https://developer.atlassian.com/bitbucket/descriptor/
[HTTP/2]: https://http2.github.io/
[xdm]: https://en.wikipedia.org/wiki/Web_Messaging
[ClojureScript]: https://github.com/clojure/clojurescript
[core.async]: https://github.com/clojure/core.async
[threading]: https://clojuredocs.org/clojure.core/-%3E
[uberjar]: https://github.com/technomancy/leiningen/blob/master/doc/TUTORIAL.md#uberjar
[AOT]: http://clojure.org/compilation
[curl]: http://curl.haxx.se/
[bitbucket-js]: https://developer.atlassian.com/bitbucket/concepts/javascript-api.html
[Closure]: https://en.wikipedia.org/wiki/Google_Closure_Tools
[cljs-rationale]: https://github.com/clojure/clojurescript/wiki/Rationale
[clj-peristent]: https://en.wikipedia.org/wiki/Persistent_data_structure
[cljsbuild]: https://github.com/emezeske/lein-cljsbuild
[Figwheel]: https://github.com/bhauman/lein-figwheel
[macros]: http://clojure.org/reference/macros
[hello-connect]: https://bitbucket.org/ssmith/hello-connect/
[hello-connect-descriptor]: https://bitbucket.org/ssmith/hello-connect/src/4810770708e659b6c20dac0e021740f56238dc06/resources/views/atlassian-connect.json.selmer?at=master&fileviewer=file-view-default
[part1]: /blog/2015/12/clojure-connect-part-1/
[part2]: /blog/2015/12/clojure-connect-part-2/
[part3-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-3
[part3]: https://developer.atlassian.com/blog/2016/01/clojure-connect-part-3/
[part4-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-4
[part4]: https://developer.atlassian.com/blog/2016/01/clojure-connect-part-4/
[part5]: https://developer.atlassian.com/blog/2016/01/clojure-connect-part-5/
[part6-code]: https://bitbucket.org/ssmith/hello-connect/commits/tag/part-6
