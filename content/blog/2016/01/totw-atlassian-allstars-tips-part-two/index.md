---
title: "Tip of the Week - The Atlassian AllStars tips and tricks for JIRA - Part Two."
date: "2016-01-14T9:00:00+07:00"
author: "pvandevoorde"
categories: ["totw"]
lede: "To continue our tips and tricks series. I want to share some more great tips
 and tricks I learned from the Atlassian AllStars. I'm sure you can learn something new too! This week I'll be sharing some more JIRA related tips."
---
This week's article is the second part in a series with tips and tricks from real Atlassian users.
You can read the first part of this series here: [Tip of the Week - The Atlassian AllStars tips and tricks for JIRA - Part One.](https://developer.atlassian.com/blog/2016/01/totw-Atlassian-AllStars-Tips-Part-One?utm_source=dac&utm_medium=blog&utm_campaign=totw "The Atlassian AllStars tips and tricks part one.")

We call these users the Atlassian AllStars.
They are an online community of Atlassian users, experts, and fans.
This community offers opportunities to learn more about our products,
 to take part in exclusive surveys and interviews, to leverage content and to connect to like-minded fans.

If you think you have what it takes to become an Atlassian AllStar, [apply here](https://atlassian.influitive.com/users/sign_up "Atlassian AllStar Application.")!

***

This week we will focus on tips and tricks for JIRA Admins and power users.

###<table><tr><td>![James Strangeway](james-strangeway.png "James Strangeway")</td><td>[James Strangeway](https://twitter.com/jimmydave23 "James Strangeway")</td></tr></table>

>Create an [email subscription](https://confluence.atlassian.com/jira/receiving-search-results-via-email-185729664.html?utm_source=dac&utm_medium=blog&utm_campaign=totw#ReceivingSearchResultsviaEmail-SubscribingtoaFilter "Email subscription to an issue filter.") to an issue filter where priority is critical and the issue is unassigned. Set it to email a support group every 15 minutes (or x minutes) until someone takes the issue. This has been great for us in identifying high priority issues.

###<table><tr><td><img src="rachel-wright.png"/></td><td>[Rachel Wright](https://twitter.com/rlw_www "Rachel Wright") </td></tr></table>

> Don't forget to set the ["Fire an event that can be processed by the listeners"](https://confluence.atlassian.com/adminjiraserver070/advanced-workflow-configuration-749383189.html?utm_source=dac&utm_medium=blog&utm_campaign=totw#Advancedworkflowconfiguration-notificationsUsingapostfunctiontosendemailnotifications "Documentation about fire event post function.") post function on each transition in your workflow. This will make sure that the correct notifications will be sent out.

###<table><tr><td>![Chris Taylor](chris-taylor.png "Chris Taylor")</td><td>[Chris Taylor](https://twitter.com/AgileTester "Chris Taylor")</td></tr></table>

>Requirements traceability between Confluence and JIRA has been useful for keeping our requirements in one place as a single source of truth. Using the [Requirements Blueprint](https://confluence.atlassian.com/doc/product-requirements-blueprint-329975392.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Confluence Product Requirements Blueprint") in Confluence we can create all our requirements (and documentation) and then [generate the JIRA issues](https://confluence.atlassian.com/doc/use-jira-applications-and-confluence-together-427623543.html?utm_source=dac&utm_medium=blog&utm_campaign=totw#UseJIRAapplicationsandConfluencetogether-CreateissuesfrominsideConfluence "Generate JIRA issues from a Confluence page.") from the requirements table.

>This automatically creates the JIRA issues based on the information in the requirements, as well as creating a two way link between the Confluence page and the JIRA issues. Now when we need to make changes to requirements we just edit the page (one single source of truth) instead of using the JIRA comments.

###<table><tr><td>![Nic Brough](nic-brough.png "Nic Brough")</td><td> [Nic Brough](https://twitter.com/Grobbendonk "Nic Brough")</td></tr></table>
>You can set up an organisation-wide simple "to do" project for personal use by individuals by using JIRA's ["Reporter Browse" permission](https://confluence.atlassian.com/jira/managing-project-permissions-185729636.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "JIRA Permission Schemes") to allow only the person creating their to-do items to see them.

###<table><tr><td>![László Murvai-Buzogány](laszlo-murvai-buzogany.png "László Murvai-Buzogány")</td><td>[László Murvai-Buzogány](https://www.linkedin.com/in/mblaszlo "László Murvai-Buzogány")</td></tr></table>
>The [JIRA REST API](https://docs.atlassian.com/jira/REST/latest/?utm_source=dac&utm_medium=blog&utm_campaign=totw "the JIRA REST API") was a major help when doing the initial migration of tens of thousands of Bugzilla issues. Only a small number of API calls and a handful generated scripts were used with great efficiency.

###<table><tr><td>![Jamie Sawyer](jamie-sawyer.png "Jamie Sawyer")</td><td>[Jamie Sawyer](https://twitter.com/JSawyerUK "Jamie Sawyer")</td></tr></table>
>My quick tip for JIRA is the [CHANGED option in JQL](https://confluence.atlassian.com/jira/advanced-searching-179442050.html?utm_source=dac&utm_medium=blog&utm_campaign=totw#AdvancedSearching-CHANGED "JQL CHANGED option").  Effectively, this allows you to find out much more information about how a particular field has changed over time.  Let's say we're a Project Admin, and we need to know which issues have moved from "In Test" to "In Progress" this week (even if they're back in testing now).  With the CHANGED function, this is simple:

>     status CHANGED FROM "In Test" TO "In Progress" AFTER startOfWeek()

***

A big thank you to all contributors, you are awesome! Be sure to come back for the next part next week!

Share your own tips and tricks in the comments or on Twitter, be sure to mention me: [@pvdevoor](http://twitter.com/pvdevoor "Peter Van de Voorde") or [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev Twitter Account") !
