---
title: "Doing the Bitbucket OAuth dance with Python"
date: "2016-02-18T12:30:00+07:00"
author: "ibuchanan"
categories: ["bitbucket", "apis", "oauth", "python"]
lede:          "OAuth brings added API security
                but it is often confusing to interpret
                the specification in light of a real server,
                while trying to figure out a new client library.
                Here is a quick-start guide for Python
                that cuts through that confusion about Bitbucket Server
                with a working example."
description:   "Ian Buchanan describes
                how developers can use Bitbucket OAuth in Python,
                using the popular Requests library
                and the requests_oauthlib extension."
---

In the world of REST APIs,
it is no longer necessary to rely on a service to provide an official SDK.
While API wrappers may add some semantic sugar,
it is often just as easy to use simple HTTP client libraries.
In Python, [Requests][requests] is often all you need
to make use of the [Bitbucket Cloud REST API][bb-api].
Just look at how simple this code is:

```python
>>> import requests
>>> r = requests.get('https://api.bitbucket.org/1.0/user', auth=('email', 'password'))
>>> r.json()
{u'repositories': [{u'scm': u'git', u'has_wiki': False, u'last_updated':
...
```

Works great!
Unless, you've enabled [two-factor authentication][2fa]
(and I hope you have).
When you have enabled two-factor authentication,
then you are no longer allowed to access Bitbucket's API
with [Basic Authentication][bb-basic-auth].
Instead, you have to use [OAuth][bb-oauth].
With the additional security comes a little more code.

## 1. Add OAuth consumer

Let's start by adding an OAuth consumer in Bitbucket.
First, find [Bitbucket Settings][settings] under your account.
Then, find OAuth under **Access management**.
Toward the bottom of the page,
you'll find the button for **Add consumer**.
On this page, enter a human-readable name and description.
In time, you may have many OAuth consumer entries,
so the name and description are mainly for you to remember what they are.
Here's a consumer I created.

<img
  src="consumer.png"
  alt="Settings... OAuth... Add consumer"
  title="Example Bitbucket OAuth Consumer"
  style="
    border: 1px solid #7C7C7C;
    border-radius: 5px;
    box-shadow: 3px 3px 3px #7C7C7C;"/>

Bitbucket supports both OAuth 1.0 and 2.0.
For this example, we'll use 2.0 so we must provide a callback URL.
We'll provide `https://localhost/` even though there isn't any web application.
Also for this example, we'll enable the Account Read permission.
If you are following along,
you can come back to edit the consumer if you decide to grant your code more permissions.

After we click the Save button,
we can click on the name of the new consumer
to reveal a key and secret.
We will need these in the client code
so we'll leave the browser open to keep them handy.

## 2. Get requests_oauthlib

Fortunately, Requests already has an extension for OAuth support,
called [`requests_oauthlib`][requests-oauthlib].
I prefer to install libraries into [Python virtual environments][virtualenv].
In any case, we can install the library with `pip install requests_oauthlib`.
For Bitbucket, the version of this library is important.
As of writing, the latest version (v0.6.0) is required
because it contains [my patch][patch] to make it work with Bitbucket.

Next, you'll want to grab the following [snippet][python-snippet]:

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from requests_oauthlib import OAuth2Session

class ClientSecrets:
    '''
    The structure of this class follows Google convention for `client_secrets.json`:
    https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
    Bitbucket does not emit this structure so it must be manually constructed.
    '''
    client_id = "Shown as Key in the Bitbucket Cloud UI"
    client_secret = "Shown as Secret in the Bitbucket Cloud UI"
    redirect_uris = [
      "https://localhost"  # Used for testing.
    ]
    auth_uri = "https://bitbucket.org/site/oauth2/authorize"
    token_uri = "https://bitbucket.org/site/oauth2/access_token"
    server_base_uri = "https://api.bitbucket.org/"


def main():
    c = ClientSecrets()
    # Fetch a request token
    bitbucket = OAuth2Session(c.client_id)
    # Redirect user to Bitbucket for authorization
    authorization_url = bitbucket.authorization_url(c.auth_uri)
    print('Please go here and authorize: {}'.format(authorization_url[0]))
    # Get the authorization verifier code from the callback url
    redirect_response = raw_input('Paste the full redirect URL here:')
    # Fetch the access token
    bitbucket.fetch_token(
      c.token_uri,
      authorization_response=redirect_response,
      username=c.client_id,
      password=c.client_secret)
    # Fetch a protected resource, i.e. user profile
    r = bitbucket.get(c.server_base_uri + '1.0/user')
    print(r.content)

if __name__ == '__main__':
    main()
```

To run this example,
you'll need to replace the `client_id` string
with the key you obtained
from creating an OAuth consumer in Bitbucket.
And the `client_secret` with the secret.

## 3. Run it

When we run the code,
we are prompted to go to a URL to approve.

```bash
Please go here and authorize: https://bitbucket.org/site/oauth2/authorize?response_type=code&client_id=bWmhNJ89sM5nzPv6P&state=9RkeiUw4d8NswlgMIaSfTYdsHo97x
Paste the full redirect URL here:
```

(If you use try to use the exact link shown above,
it won't match the `client_id` and `state` provided by any real consumer
so it won't work.)

Once you click the approve button,
your browser will redirect to localhost with a query parameter.
Unless you are actually running a web application on your machine,
you should just get a simple 404 error from your browser.
**That's expected.**
Just copy the URL from the browser and paste it back to your command-line program.
It might look something like this:

```
https://localhost/?state=9RkeiUw4d8NsSwlgMIaSfTYdsHo97x&code=c5LQuyquUyDT3ysSgr
```

That was the OAuth dance,
so the code just spits out some JSON for a normal REST API request.

## That's all?

I wish.
If you were writing a command-line application,
there are better grant types than the default one for `requests_oauthlib`.
That flow is the first one in the OAuth 2.0 specification,
known as [Authorization Code Grant][authorization-code-grant].
It is the most popular implementation on servers,
which means it is the one flow surely supported by every OAuth library or tool.
To set a baseline with a new library or tool,
I always start with this flow.
However, popularity doesn't mean
the authorization code grant flow is good for command-line applications.
Although we saw how it can work,
all the copy paste stuff is error-prone and tedious.
A better grant flow would be
[Resource Owner Password Credentials Grant][password-grant].
In `requests_oauthlib`,
you'll want to learn more about the [legacy application flow][legacy-flow]
and the [LegacyApplicationClient][legacy-client].
On the other hand, if you are writing a web application,
you will replace those 2 awkward copy-paste steps need
with real web interactions.
The first, where users authorize, can just be a web redirect.
The second, where users return with an authorization code,
should redirect to a real URL within your application, not `localhost`.

Also, this contrived example only made a single HTTP request.
If our code is going to make multiple requests over time,
we would need to account for the fact that OAuth tokens time-out
and need to be refreshed.
In `requests_oauthlib`,
you'll want to learn more about [refreshing tokens][refresh-token].

I hope you find that a useful starting point.
If you have questions or need other help working with the Bitbucket API in Python,
tweet me at [@devpartisan][devpartisan] or my team at [@atlassiandev][atlassiandev].



[requests]: http://docs.python-requests.org/en/latest/ "Requests is an Apache2 Licensed HTTP library, written in Python, for human beings."
[bb-api]: https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+Cloud+REST+APIs "Documentation for the Bitbucket Cloud REST APIs."
[2fa]: https://confluence.atlassian.com/display/BITBUCKET/Two-step+verification "Documentation for Bitbucket Cloud's two-step verification, also known as two-factor authentication."
[bb-basic-auth]: https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+Cloud+REST+APIs#UsetheBitbucketCloudRESTAPIs-Authentication "Documentation on Bitbucked Cloud's Basic Authentication for the REST APIs."
[bb-oauth]: https://developer.atlassian.com/bitbucket/concepts/oauth2.html "Documentation for Bitbucket Cloud OAuth 2.0"

[settings]: https://bitbucket.org/account/ "Your account settings."

[virtualenv]: http://docs.python-guide.org/en/latest/dev/virtualenvs/ "A Virtual Environment is a tool to keep the dependencies required by different projects in separate places, by creating virtual Python environments for them. It solves the 'Project X depends on version 1.x but, Project Y needs 4.x' dilemma, and keeps your global site-packages directory clean and manageable."
[requests-oauthlib]: https://requests-oauthlib.readthedocs.org/en/latest/ "Documentation for requests_oauthlib."
[patch]: https://github.com/requests/requests-oauthlib/pull/206 "Ian Buchanan (19 November 2015). Added authorization header for token access."
[python-snippet]: https://bitbucket.org/snippets/ian_buchanan/78dGy/example-using-pythons-requests_oauthlib "Ian Buchanan (27 January 2016). Example using Python's requests_oauthlib for Bitbucket Cloud REST API."

[authorization-code-grant]: https://tools.ietf.org/html/rfc6749#section-4.1 "RFC 6749: OAuth 2.0. Section 4.1. Authorization Code Grant."
[password-grant]: https://tools.ietf.org/html/rfc6749#section-4.3 "RFC 6749: OAuth 2.0. Section 4.3. Resource Owner Password Credentials Grant."
[legacy-flow]: https://requests-oauthlib.readthedocs.org/en/latest/oauth2_workflow.html#legacy-application-flow "Documentation on the Legacy Application Flow."
[legacy-client]: http://oauthlib.readthedocs.org/en/latest/oauth2/clients/legacyapplicationclient.html#oauthlib.oauth2.LegacyApplicationClient "Documentation on the LegacyApplicationClient."
[refresh-token]: https://requests-oauthlib.readthedocs.org/en/latest/oauth2_workflow.html#refreshing-tokens "Documentation on Refreshing tokens."

[devpartisan]: https://twitter.com/devpartisan "Ian Buchanan is @devpartisan on Twitter."
[atlassiandev]: https://www.twitter.com/atlassiandev "Atlassian's Developer Advocates for Developer Tools are @atlassiandev on Twitter."
