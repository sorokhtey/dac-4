---
title: "Get your kids coding with Minecraft"
date: "2016-02-03T14:50:00+07:00"
author: "gfrancisco"
description: "This first post discusses getting started as a parent helping your child get excited with programming in the Minecraft environment."
categories: ["javascript", "Minecraft", "CanaryMod", "ScriptCraft"]
lede: "Do you want to take advantage of your kids fascination in Minecraft and get them into coding? Hook them and get them going quickly with some JavaScript using CanaryMod and ScriptCraft."

---
<style>
    img.screenshot {
      margin-top: 10px;
      margin-bottom: 10px;
      margin-left: auto;
      margin-right: auto
    }
</style>

If you have young children like I do between the ages of 5-10 you're probably
all too familiar with the Minecraft craze and may even have the books and foam
toys related to Minecraft in your home. In the interest of trying to find some
educational value out of this block building fascination I started looking into
how you would get kids interested in coding alongside their Minecraft gaming. We
did do some [Scratch](https://scratch.mit.edu/) programming but it really didn't
hold their interest the way Minecraft has so I felt it was more important to
find a way to piggy back off that. There are books and classes on modding with
Minecraft using Java. I felt that might be too complex for my young kids at this
point and stumbled on ["The Young Person's Guide to Programming in
Minecraft"](https://github.com/walterhiggins/ScriptCraft/blob/master/docs/YoungPersonsGuideToProgrammingMinecraft.md).
There's a ton of valuable content in this guide but I also quickly realized it
wasn't aimed at children in this age group, but felt it would still be valuable
to provide some tips for parents wanting to leverage some of this content to get
their young kids excited about coding. And for those of you wondering, yes I'll
also be looking to do a pull request to contribute some of this content directly
on the [ScriptCraft repository](https://github.com/walterhiggins/ScriptCraft/pulls).
##Initial Hurdles

###Installation
This is an area of the guide which I did not do with my children. At this age I
wanted to make sure we could focus on "the fun stuff" and show them the
immediate gratification of coding versus setting things up so you can get to the
coding part. I'm glad I did that because there were some quirks in setting this
up that I had to sort through which would have easily frustrated the kids. Note
that we did this on a Mac.

####Pre-reqs
+ Use a normal account to install everything. I originally tried to do this
with parental controls on for my child's account. Unfortunately it restricts use
of both Minecraft and command line calls you'll need to use. Admin level is not
necessary for the account.
+ Get the full client version of Minecraft. Because the kids had been playing
Minecraft Pocket Edition (PE) on iPads, it wasn't immediately obvious
that we wouldn't be able to connect from that version to the modified Minecraft
server we were setting up with CanaryMod. You will need to purchase that on the
[Minecraft site](https://minecraft.net/), and you may get a potential fraud
alert from your credit card company afterwards. Make sure to use a secure
password.
+ Install [Java](https://www.java.com/en/download/help/mac_install.xml) if you don't
already have it: It's a prerequisite for running the modified server.

Links for the latest:
Make sure you are using versions that are compatible with each other else the
ScriptCraft plugin will not load properly. These are the ones I used:
+ [CanaryMod-1.2.0_0.jar](https://canarymod.net/releases/CanaryMod-1.2.0_0.jar)
+ [scriptcraft-3.1.12](http://scriptcraftjs.org/download/latest/scriptcraft-3.1.12/)

###Starting things up
One basic thing to note is when the instructions tell you to "start the server",
they mean the following:
1. Open terminal
2. Navigate to the folder
3. Type `java -jar CanaryMod-1.2.0_0.jar`

If all goes well you should see:

![fullstartwithplugin](fullstartwithplugin.png)

Note that you should see "Found 1 plugin" if you also installed the ScriptCraft
plugin correctly. The status ends with a command prompt for the running server.
To exit gracefully from the server when you are done, go back to terminal and
simply type "stop". If you don't do that and close terminal or Ctrl-z the
related java process will hang onto the port which CanaryMod was started on -
the default being 25565 and cause you problems when you try to start the server
again.


###Navigating around Minecraft
If you are not a regular Minecraft player as was my case, make sure you know how
to [navigate](http://minecraft.gamepedia.com/Controls) around the game.

The one thing I could not find in that reference or in basic Google searches was
how to open a door without destroying it. I eventually came across a tip to
Ctrl + click my way through. Then you can keep your pretty doors on your cottages
and other structures :). Strangely the kids were used to just destroying the
doors to get into structures. Hmmmm....

###Getting yourself going
When you launch the Minecraft client for the first time follow the instructions
to set up for multiplayer. The guide says to use just "localhost" for the
connection but you actually have to use `localhost:<port>` where port is
either the default port that is defined for CanaryMod which is 25565 or whatever
port you may have alternatively defined in the CanaryMod configuration file.


<img class='screenshot' alt='clientcfg' src='clientcfg.png'
     title='clientcfg' width=500px>

Ok then, the guide refers to posting some text in the commandline. Note that
there is a commandline within the Minecraft client itself, as well as in the
terminal where you started your CanaryMod server. There will be occasions in
the client where it seems like your keyboard strokes are not all going through.
My oldest calls it "when it gets glitchy". That's a good label. :) You will have
to restart your Minecraft client in that case and sometimes also your CanaryMod
server.

If you don't want to get distracted with the monsters and other creatures that
come out to kill your character, make sure to set your gamemode right away to
creative. That can be done both in the [config
file](https://github.com/walterhiggins/ScriptCraft/blob/master/docs/YoungPersonsGuideToProgrammingMinecraft.md#configuring-your-server-optional)
as well as by typing on the cmdline within Minecraft: `/gamemode c` . This will
help you and your kids focus on programming and playing versus trying to
survive.

###Getting your kids going

First start with making sure ScriptCraft is working:

```
/js 1 + 1
```

We started off with the exercises in the guide which had us creating blocks
programmatically with a one line call:

```
/js box(5,3,2,4)
```

![boxinair](firstboxinair.png)

This created a box made of oak with an id of 5 and with the specified width,
depth, and height. This was created in the air because it started from where
the cross hairs are pointing.

One of the kids immediately asked if we could spawn creatures like cows and
horses:

```
/jsp spawn cow
/jsp spawn horse
```

These calls were different than the ones we used to create blocks which prefaced
with /js not /jsp. Doing some digging on this Walter added [this mechanism](http://walterhiggins.net/blog/ScriptCraft-1-Month-later)
so that regular players could make some calls intentionally exposed to them.
Remember the "op" command? That enables you to make powerful calls using /js
which you shouldn't enable for all players.

When it starts to rain or get dark in the game you can also show some quick
command prompts to give your child back a clear and sunny day:

```
/time set 1000
/weather clear
```

##The Parent Demo

I found it challenging getting my kids to pause long enough in their Minecraft
gaming to do some simple exercises to get them engaged and excited about
programming. This is where you spend some time understand what your kids are
doing with Minecraft and start looking for examples of things to point out can
be done so much faster with just a bit of code. The following are some
suggestions for you to get them going:

+ Show them how to create a cylinder made of cake blocks using the cylinder
function which takes block id, radius and height. My oldest was
absolutely floored that you could simulate cylinders in this block world and on
top of that make it out of cake (id 92). She switched to survival mode by typing
`game mode s` and proceeded to eat  as much cake as possible which apparently
replenishes your health in Minecraft. If only that were true in real life. :)

```
/js cylinder(92, 5, 20)
```

Here's an aerial view of the result:

![cake](cakecylinder.png)
+ Create a cottage, then a set of cottages, a fort, and a castle. Remember to
move the crosshairs so that you aren't building these structures on top of
each other.

```
/js cottage()
/js cottage_road()
/js fort()
/js castle()
```

![castle](castle.png)

+ Then show them how to change the flooring of one these
structures to slime. My kids tipped me off that if we do that your player then
can joyfully fly through the clouds and land in a virtual bounce house. Slime
blocks weren't listed in Walter's guide but you can find the entire list on
[gamepedia](http://minecraft.gamepedia.com/Data_values/Block_IDs). Slime is id
165.

+ And you can even add a dance floor in one of your structures

```
/js dancefloor()
```

![dancefloor](dancefloor.png)

+ Clear a large field to make it easier to place structures. It wasn't obvious
how to do this and even after searching on various Minecraft sites there wasn't
a clear answer outside of a popular mod called WorldEdit. I didn't want
to risk installing a mod I didn't know much about and eventually I stumbled on
some code in the ScriptCrafts plugin folder that used air blocks. Yes air
blocks! So here you go:

```
/js box(blocks.air, 50, 50, 50)
```

![clearland](clearland.png)

This clears a large area and can take up to a minute to run so be patient. The
kids thought this was wonderfully cool. If you have tall trees or mountains in
that space you may end up with some floating remnants in the sky but you can
reapply the code in the air as well by flying up to that section and
repositioning your crosshairs. We could have also cut to the chase and
[set up the server as a flat world](http://walterhiggins.net/blog/CanaryMod-and-ScriptCraft-Quick-Start)
to begin with but that would have taken some of the fun out.  Note that if you
try numbers higher than 50 for those dimensions you may end up crashing your
server.

Ok - so finally you've got them interested and asking questions about what
else you can do. Now is a good time to explore other calls you can make and even
create your own mod. Here are some links to get you going.

+ [ScriptCraft home site](http://scriptcraftjs.org/)
+ [Your first Minecraft Mod](https://github.com/walterhiggins/ScriptCraft/blob/master/docs/YoungPersonsGuideToProgrammingMinecraft.md#your-first-minecraft-mod)
+ [ScriptCraft API Reference](https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md)
+ [Walter's introductory blog post on ScriptCraft](http://walterhiggins.net/blog/ScriptCraft)


I'll be covering our adventures in mod programming in the next post and would
love to hear how yours goes too.
***
Ping me on twitter [@gracefr](http://twitter.com/gracefr "Grace Francisco")
and follow us on  [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev
Twitter Account").
