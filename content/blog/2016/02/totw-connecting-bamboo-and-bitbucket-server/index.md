---
title: "How to connect Bamboo and Bitbucket Server"
date: "2016-02-16T12:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Bamboo","Bitbucket"]
lede: "This week I'll show you how to connect Bamboo and Bitbucket Server. And how this integration will help you build better software faster."
description: "Tutorial on how to connect Atlassian Bamboo and Bitbucket Server."
---
This week I'm going to talk about connecting Bamboo and Bitbucket Server.
And what this means for your software development process.

## Installation and Configuration

We'll start with a clean install of Bamboo and Bitbucket Server.
You can download them here:
- [Atlassian Bamboo](https://www.atlassian.com/software/bamboo/download/?utm_source=dac&utm_medium=blog&utm_campaign=totw "Download the latest version of Atlassian Bamboo.")
- [Atlassian Bitbucket Server](https://www.atlassian.com/software/bitbucket/download?utm_source=dac&utm_medium=blog&utm_campaign=totw "Download the latest version of Atlassian Bitbucket Server.")

And simply follow the install instructions:
- [Bamboo installation instructions](https://confluence.atlassian.com/bamboo/bamboo-installation-guide-289276785.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Atlassian Bamboo Installation Instructions.")
- [Bitbucket Server installation instructions](https://confluence.atlassian.com/bitbucketserver/getting-started-776640896.html "Atlassian Bitbucket Server Installation Instructions.")

For demo and testing purposes the embedded database install is perfect.
Be aware that you might need to install certain prerequisites first!

Once you've installed both the tools we need to connect them, for this we need to set-up an application link. Take a look at [Integrating Bamboo with Bitbucket Server](https://confluence.atlassian.com/bamboo/integrating-bamboo-with-stash-289276959.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Integrating Bamboo with Bitbucket Server.") to see how you can set this up.

## Get some data into your tools.

Now you can import a test project into your Bitbucket Server.
I choose the [run-bucket-run](https://bitbucket.org/tpettersen/run-bucket-run?utm_source=dac&utm_medium=blog&utm_campaign=totw "Bitbucket run-bucket-run repository.") project from my colleague [Tim Pettersen](http://twitter.com/kannonboy "Tim Pettersen") which is hosted on Bitbucket Cloud.

Use these instructions to clone the repo to your local instance: [Tip of the Week - How to move a full Git repository.
](https://developer.atlassian.com/blog/2016/01/totw-copying-a-full-git-repo?utm_source=dac&utm_medium=blog&utm_campaign=totw "Tip of the Week: How to move a full Git repository")

Once this has been done we can create a Bamboo build plan for our Bitbucket repo.

Go to Bamboo start creating a new plan, here is my configuration:

![A new build plan](creating-a-new-build-plan.png "Bamboo Create Build Plan Setup.")

Due to the fact that Bitbucket Server and Bamboo are linked I can immediately select my repository.


The next step is adding [jobs and tasks](https://confluence.atlassian.com/bamboo/jobs-and-tasks-289277035.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Atlassian Bamboo Jobs and Tasks") to the build plan.

For the purpose of this example I simply do a source code checkout:

![Single Task](single-task.png "Source code checkout Task in my Bamboo Build Plan.")

**Before clicking on create be sure to activate your build plan!**

Immediately a first build will be triggered and displayed in Bamboo:

![First Build](first-build.png "First build in Bamboo.")
This build plan will be triggered every time you commit something to the repository.

You will also see the build result on each commit in Bitbucket Server.
And be able click through to the detailed build results in Bamboo:

![See build results in Bitbucket Server](builds-in-bitbucket-server.png "Bamboo Build Results in Bitbucket Server.")

And you will be able to see all commits in a build inside Bamboo.
Clicking it will instantly bring you to the relevant commit or diff in BitBucket Server.:

![See Bitbucket Server commits in Bamboo](commits-in-bamboo.png "See Bitbucket Server commits in a Bamboo Buid Plan")

This is only the beginning! Let me show you what [Plan Branches](https://confluence.atlassian.com/bamboo/using-plan-branches-289276872.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "Bamboo Plan Branches") can do for you.

Configuring plan branches is as easy as going to your build plan configuration, go to the branches tab and selecting **Create plan branches for all new branches** in the dropdown next to the label **New Branches**:

![Configuring Plan Branches in Bamboo](plan-branches.png "Configuring plan branches in Bamboo.")

Now Bamboo will create a plan for each new branch it will detect in your repository.
This build plan will be a copy of your original build plan but will have it's own logs, artifacts and triggers. Out of the box it will be triggered by a commit on the branch itself.
So let's create a new branch **feature-totw** to test this out.
Here is how Bamboo looks like after we've created the new Branch:

![Select Plan Branches in Bamboo](plan-branches-select.png "Dropdown to select a plan branch in Bamboo.")

As you can see we can now select 2 branches (Master and feature-totw), this is what we see when feature-totw is selected:

![Plan first build](first-build.png "Our branches first build.")

It's a complete build plan overview for this new branch.
If you have several branches at the same time you can even get a nice overview off how their builds are going in Bamboo:

![Plan Branches overview](plan-branches-overview.png "The plan branches overview in Bamboo.")

This is very cool if you are working with a big team and using feature branches, you can immediately see which features are breaking their builds.

And this is not everything plan branches can do for you. If have been paying attention you would also have noticed the **Merging** title on the plan branches configuration tab.
This option will give you the possibility to either update your branch with the latest commits from another branch (**[Branch Updater](https://confluence.atlassian.com/display/BAMBOO059/Using+plan+branches#Usingplanbranches-Gatekeeper)**) or push your changes to another branch (**[GateKeeper](https://confluence.atlassian.com/display/BAMBOO059/Using+plan+branches#Usingplanbranches-Gatekeeper)**). Both of these actions will happen after a successful build.

![Merging Options](merging-options.png "Merging Options.")

These provide you with some extra tools to make working with feature branches so much easier.
Simply use Branch Updater to keep your feature branch up-to-date with the latest changes on Master and you will never have to worry about suddenly breaking a build when you merge your feature into Master.

Here is the result of Branch Updater in Bamboo:

![Branch Updater at work in Bamboo](branch-updater-at-work.png "Branch Updater at work in Bamboo.")

And here is what you see in Bitbucket Server:

![Branch Updater at work in Bitbucket Server](branch-updater-bitbucket.png "Branch Updater at work in Bitbucket Server.")

So this is all Bamboo, but what about Bitbucket Server?
You already saw the build result that are available in Bitbucket Server and the automatic merges created by Bamboo.
But what else is there?

You can use successful builds as a requirement in Pull Requests! Simply go to your repository settings and select the Pull requests menu item. Here you can set the number of successful builds a pull request needs before it can be merged.

![Pull Request approval based on number of builds](pr-builds.png "Pull Request approval based on number of successful builds.")

And if you try to merge a pull request before you have to right number of successful builds you'll see this:

![Pull Request cannot be approved because of number of builds](more-builds-needed.png "More builds needed for Pull Request to be merged.")

So this helps you again to be absolutely sure that anything you will merge into Master will not break your Master Build and will have no impact on your releases.

Share your own tips and tricks in the comments or on Twitter, be sure to mention me: [@pvdevoor](http://twitter.com/pvdevoor "Peter Van de Voorde") or [@atlassiandev](http://twitter.com/atlassiandev "Atlassian Dev Twitter Account") !
