---
title: "The Atlassian Marketplace API: stable, feature-rich, and documented!"
date: "2016-05-24T13:00:00+07:00"
author: "bwoskow"
categories: ["marketplace", "apis", "atlascamp"]
lede: "The Atlassian Marketplace team is proud to announce the latest version of our REST API. This API is stable, supported, provides the ability to access and manage multiple aspects of the site, and best of all - is fully documented!"
---

Today at <a href="https://www.atlassian.com/atlascamp" target="_blank">AtlasCamp</a>, we announced the latest version of our <a href="https://marketplace.atlassian.com" target="_blank">Atlassian Marketplace</a> API. This REST API is stable, supported, provides the ability to access and manage multiple aspects of add-on listings and vendor businesses, and best of all - is [fully documented](https://developer.atlassian.com/market/developing-for-the-marketplace/marketplace-api).

## What can the API do for me?

This REST API is designed to integrate Atlassian Marketplace directly into your business process and tooling. Let's look at an example API usage. One of the simplest ways to use the API is to retrieve an add-on's details. We can do so with the following cURL command:

``` bash
# Get the details for "Portfolio for JIRA", along with the current version
curl "https://marketplace.atlassian.com/rest/2/addons/com.radiantminds.roadmaps-jira?withVersion=true"
```

What about something more useful, for example, a request to get a list of all licenses for customers who have recently purchased your add-ons:

``` bash
# Get a vendor's add-on licenses which have been updated (purchased, renewed, etc.) on or after a specified date.
# Note: this request requires a valid username, password, vendor id and a date
curl -u USERNAME:PASSWORD "https://marketplace.atlassian.com/rest/2/vendors/VENDOR_ID/reporting/licenses?lastUpdated=YYYY-MM-DD"
```

It gets even better! The `licenses` resource from this example supports a number of request parameters to allow for advanced license searching. Keep reading to find out more about these parameters and how to use them.

That's just the start.
 
## Is the API easy to use?

Yes, this REST API is easy to use for a number of reasons. First of all, it follows the [HAL specification](https://tools.ietf.org/html/draft-kelly-json-hal-08) to provide clear and consistent behavior. Most notably, the API's responses include link maps (`_links`) to reference related resources and embedded representations (`_embedded`) to clearly denote when sets of properties belong to a different-yet-related object.

Using the link maps, clients can easily navigate between resources to access all of the desired data. Additionally, resources which support a number of optional request parameters will be represented as *link templates* to provide a programmatic interface with which clients can determine how to use it.

As with most REST APIs, you're free to use your favorite REST client; my favorites are [Chrome's Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) and [cURL](https://en.wikipedia.org/wiki/CURL). Alternatively, if you're accessing the API in a JVM-based language, you can use the [Marketplace Java client](https://developer.atlassian.com/market/developing-for-the-marketplace/marketplace-api/using-the-marketplace-api-java-client).

And of course, the API is now finalized and stable &mdash; meaning that you can build on top of it with confidence.
 
## Where can I get started?

You can access <a href="https://marketplace.atlassian.com/rest/2" target="_blank">the API</a> directly and navigate through the links map. Just use a browser or your favorite REST client. Alternatively, you can view our extensive [API documentation](https://developer.atlassian.com/market/developing-for-the-marketplace/marketplace-api). 

The majority of our documentation is structured with detailed information about request and response formats. This part of our documentation is built with [Swagger](http://swagger.io) and deployed with [RADAR](https://bitbucket.org/atlassian/radar), a new API documentation generator that we've built to present this and other Atlassian APIs. Additionally, we have open-sourced RADAR as part of [joining the Open API Initiative](https://developer.atlassian.com/blog/2016/05/open-api-initiative/).

Lastly, you can follow the [tutorials](https://developer.atlassian.com/market/developing-for-the-marketplace/marketplace-api#MarketplaceAPI-Moreinformation) to learn more about how to use the Marketplace API.
 
## What about the previous API version?

<a href="https://marketplace.atlassian.com/rest/1.0" target="_blank">Version 1.0</a> of the Atlassian Marketplace API will remain stable and accessible. However, future improvements and bug fixes may only apply to the newer version.

We hope that you try out the new API! Please provide us with any feedback that you have in the Marketplace team's [JIRA project](https://ecosystem.atlassian.net/browse/AMKT).
