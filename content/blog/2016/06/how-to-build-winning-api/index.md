---
title: "From beta to GA: How to build a winning API"
date: "2016-06-06T16:00:00+07:00"
author: "tcrusson"
categories: ["apis"]
---

When you think of an API, you probably don't picture a bunch of drawings on a
whiteboard. But exactly one year ago, our plans for HipChat Connect were just
that. In a year this idea evolved through several stages, and finally made it
to public GA with [over 30 partners]. We've learned a lot about shipping and
partnership in the process, and we hope what we've learned will be helpful to
other software development teams (like yours, maybe).

Like many of you, we believe APIs are the gold standard of product and
ecosystem development today. Having a platform that connects your product to a
larger market can have huge benefits for your business, your partners who
build on the platform, and your end customers. It's a win-win-win. But with
the sheer amount of players involved, developing an API is a unique and
challenging experience.

## About Our API

For context, a little bit about [HipChat Connect]. It's an API that enables
developers to write conversational apps for HipChat that are fully embedded in
the chat window. You can add buttons, dialogs, forms, and interfaces, all
using your own technology stack. It's the first implementation of Atlassian
Connect to be supported both in the cloud and server versions of the product,
and works on the web, Mac, Windows and Linux HipChat Apps. Today, more than
[35 HipChat Connect integrations][hipchat integrations] are available in the
[Atlassian Marketplace], with more to come.

## About shipping

### 1. Ship fast and iterate

Shipping an API is very different from shipping a product: once it's out
there, it's out there. Forever. That said, doing things right should never be
at the cost of speed. You need developers using your API early on in order to
make it competitive. Shipping fast means cutting scope often, but it shouldn't
mean cutting corners. Whatever choices we made, we always decided against
solutions that would hold us back later on.

A few key decisions we took to help with this strategy:

- We **cut scope** aggressively and only kept features
that demonstrated the core value of the product.
- We took the **fastest path to delivery.**
During the beta stage we made HipChat Connect invitation-only for developers,
and opt-in only for administrators, with a lot of limitations.
- We **gathered data and feedback** from early adopters,
through in-product prompts linking to Google forms for end users,
and a dedicated HipChat group where we talked to developers.
Among other things, it helped us identify early
what approaches would and wouldn't work at scale.

<style>
  .timeline {
    width: 800px;
    margin: 20px;
    border-collapse:collapse;
    border-spacing:0;
  }
  .timeline-desc {
    font-size: 18px;
  }
  .six-weeks {
    color: rgb(128,0,0);
    font-size: 24px;
    font-weight: bold;
  }
  .three-months {
    color: rgb(255,0,0);
    font-size: 24px;
    font-weight: bold;
  }
  .six-months {
    color: rgb(255,153,0);
    font-size: 24px;
    font-weight: bold;
  }
  .twelve-months {
    color: rgb(0,128,0);
    font-size: 24px;
    font-weight: bold;
  }
</style>

<table class="timeline">
  <col width="20%">
  <col width="25%">
  <col width="55%">
  <tr class="timeline-row">
    <td class="six-weeks">6 weeks</td>
    <td class="timeline-desc">Prove it's a thing</td>
    <td class="timeline-desc">The time to build and ship the internal alpha</td>
  </tr>
  <tr class="timeline-row">
    <td class="three-months">3 months</td>
    <td class="timeline-desc">Get it out there</td>
    <td class="timeline-desc">The time to the external alpha</td>
  </tr>
  <tr class="timeline-row">
    <td class="six-months">6 months</td>
    <td class="timeline-desc">Make it work</td>
    <td class="timeline-desc">The time to the public beta</td>
  </tr>
  <tr class="timeline-row">
    <td class="twelve-months">12 months</td>
    <td class="timeline-desc">Make it scale</td>
    <td class="timeline-desc">The time to GA</td>
  </tr>
</table>

### 2. Go cloud first

What we all love about cloud is that a cloud MVP can be much, much, leaner
than a server-based MVP. You don't have to live with the impact of your
mistakes as long, you can run experiments and collect data to validate ideas,
and you can decide the speed at which you release with techniques like feature
flags. And, if you combine this with allowing each developer to release to
production, with the unit of change being a pull request, it basically means
you can test an idea in days instead of weeks. In our case, the first version
of Connect in HipChat Cloud was out in 6 weeks, while [the first version] of
Connect in HipChat Server took 12 months.

### 3. Dogfood it to death

To have any chance of shipping a public API in time, we knew we needed a lot
of developers to use (and abuse) the API early on. There are several ways you
can do this, including recruiting another team within your company or reaching
out to developers from your favorite [Meetup] group. We started testing within
our own company, and shipped an internal alpha to Atlassians to build on
during our quarterly [ShipIt] hackathon. This first step gave us a ton of good
feedback to mature the API.

Having dogfooded internally, our next step was to take it to the streets, and
by streets, we mean, an external Alpha for partners. Our short timeframe
required outside-the-box thinking, which resulted in a Vendor Lab Week, where
we invited 12 companies to our office for a week-long hackathon. This was one
of the highlights of the project. Our office was buzzing with excitement and
there were some great demos unveiled that week! It's one thing to explain your
vision to companies, but quite another when they're selling it back to you. If
this sort of event is within your realm of possibility, we highly recommend
it.

## About building partnerships

Launching an API without partners is meaningless, that much is clear. But what
isn't always clear is who the right partner would be. You can't just team up
with whoever is out there; it needs to be a mutually beneficial partnership.
So, before spreading the word, sit down and define the objectives that will
help you land the right partners. Then, do everything you can to win them over
and serve their needs.

### 1. Win your partner's heart

Partners love it when you prioritize making them successful. One thing we
realized very early on was that partners want guidance when it comes to
building for you. After all, you know your products and challenges better than
they do! We worked with our partners on the design and implementation of their
add-on and it was a great experience for both parties. It forced us to think
about the core value proposition of our product and company, as well as where
third party apps could play a role. What helped us gain traction with a lot of
partners was to paint a vision for the space they work in that showed we cared
about their success.

### 2. Help your partners grow

Here's something everyone should know: shipping on a new platform doesn't
necessarily produce a sudden rush of users. This expectation can often result
in partners falling into an add-on death loop, as illustrated by David Bland's
[now famous tweet] illustrating the "Product Death Cycle":

![no one uses your add-on -> ask users what's missing -> build missing
![features -> repeat](product-death-spiral.png "Product Death Cycle")

Realizing that continually piling features on top of an existing add-on was
not going to cut it, we spent time discussing growth tactics with our
partners. We advised them to make sure their integrations could be installed
by anyone, not just an administrator. We encouraged them to delay asking for a
sign up for an add-on until after it had been evaluated. And finally, we had
them focus on onboarding, and making sure the user was given a proper tour of
the add-on after installing in order to demonstrate its value.

### 3. Don't \#@!% the partner

Your partner's success may live and die by the permissions you set up on your
API. While permissions may be set up with the best of intentions, you can
potentially hamstring your partner's integration if there's too much red tape.
For instance, if only a small subset of users can install integrations, and
only in a specific context, how can a partner be expected to grow? Take the
friction out of the process by keeping everything open by default, and
offering the ability for admins to lock it down later if they want to.

## Got it. Now what?

With APIs, you could keep building more features until the end of time! But
first you have to make sure your API is put to good use. Here are some things
to consider as you keep growing:

- Tell the success stories of partner integrations
that have really added value to the product.
- Drive up installation and usage of integrations through experimentation.
- Figure our how integrations improve customer experience in your product,
and focus on continuing to move the needle.

Well there you have it; that's our story. We're excited about what our team
has come together to build, and we're looking forward to seeing other dev
teams doing similarly great things in the world of APIs.



[over 30 partners]: https://blog.hipchat.com/2016/04/13/35-new-ways-to-do-your-work-right-inside-of-hipchat/
[HipChat Connect]: https://www.hipchat.com/connect
[hipchat integrations]: https://blog.hipchat.com/2016/04/13/35-new-ways-to-do-your-work-right-inside-of-hipchat/
[Atlassian Marketplace]: https://marketplace.atlassian.com/plugins/app/hipchat/highest-rated?cost=free&hosting=cloud
[the first version]: https://blog.hipchat.com/2016/04/26/help-us-test-the-hipchat-server-2-0-beta/
[Meetup]: http://www.meetup.com/
[ShipIt]: http://www.atlassian.com/shipit
[now famous tweet]: https://twitter.com/davidjbland/status/467096015318036480
[signup]: https://developer.atlassian.com/newsletter/
