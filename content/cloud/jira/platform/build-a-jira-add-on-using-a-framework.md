---
title: Build a JIRA add-on using a framework
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
guides: guides
aliases:
- /jiracloud/getting-started-39988011.html
- /jiracloud/getting-started-39988011.md
confluence_id: 39988011
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988011
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988011
date: "2016-09-24"
---

# Build a JIRA add-on using a framework 

Frameworks help to generate some of the plumbing required for your Connect add-on. This makes it easier to create an add-on and simplifies the development process. There are a range of Atlassian Connect frameworks available for different languages, frameworks and tools, but in this tutorial, we'll use one of the supported ones: [Atlassian Connect Express].

Atlassian Connect Express (ACE) is the official Atlassian Connect framework for Node.js. In this tutorial, you'll set up ACE, then build a basic JIRA Cloud add-on using it. This will be a fully fledged Atlassian Connect add-on, not just a web app. As a bonus, you'll be able to use it as a starting point for many of the other JIRA Cloud tutorials in this documentation.

## Before you begin

If you are new to JIRA Cloud development, read the [Getting started] guide before you start this tutorial.

To complete this tutorial, you'll need the following:

-   Your favorite IDE or text editor.
-   A running Node.js environment (v4.5.0 or later required): <a href="https://nodejs.org/en/download/" target="_blank">download Node.js</a>, if you don't have it already. Node.js bundles npm, which you'll also need.

## Install Atlassian Connect Express

In this section, you'll use **npm** to install Atlassian Connect Express (ACE).

1.  Install the atlas-connect CLI tool by running the following command:

    ``` shell
    npm i -g atlas-connect
    ```
2.  Test that atlas-connect has installed correctly by checking the version:

    ``` shell
    atlas-connect -V
    ```
     Your terminal should show something like this: `0.6.4.`

## Create a JIRA add-on using ACE

We're ready to build an add-on using ACE! This JIRA Cloud add-on will be about as basic as it gets, but you'll be able to learn the fundamental steps of the process.

1.  Create an add-on project using the `atlas-connect` command. We're using the project name "jira-getting-started" in this tutorial. Run this in any directory, except for the project directory you used for your previous add-on above.

    ``` shell
    atlas-connect new -t jira jira-getting-started
    ```
    This command will generate the basic skeleton for your atlassian-connect-express enabled add-on, in a new **jira-getting-started** directory:

    ``` shell
        .
        ├── README.md
        ├── app.js
        ├── atlassian-connect.json
        ├── config.json
        ├── credentials.json.sample
        ├── package.json
        ├── private-key.pem
        ├── public-key.pem
        ├── package.json
        ├── public
        │   ├── css
        │   │   └── addon.css
        │   └── js
        │       └── addon.js
        ├── routes
        │   └── index.js
        └── views
            ├── hello-world.hbs

            ├── layout.hbs

            └── unauthorized.hbs
    ```        
2.  Change to the **jira-getting-started** directory and install all required dependencies:

    ``` shell
    npm install
    ```

That's it! You now have an Atlassian Connect add-on. The `atlas-connect new` command actually creates a simple "Hello World" dialog in your new add-on, which you'll see when we deploy it to JIRA in the next step.

## Deploy your JIRA add-on

You have a JIRA Cloud instance and you have an add-on. It's time to put the two together. 

1.  In your **jira-getting-started** directory, copy the `credentials.json.sample` file to a new `credentials.json` file. Edit the `credentials.json` file and update the URL, username, and password to match your JIRA Cloud instance, then save it. It should look something like this:

    ``` shell
    {
        "hosts": {
            "<your development instance URL goes here>": {
                "product": "jira",
                "username": "admin",
                "password": "examplepassword"
            }
        }
    }
    ```
    {{% note %}}Note, the `username` is not the same as the email address you signed up with. The default username is "admin".{{% /note %}}    
2.  We're ready to deploy your add-on! Run the following command:

    ``` shell
    npm start
    ```
    This will boot up your Express server on the default port of 3000. Here's where you'll see more benefits of using a framework, as ACE will also do the following for you:
    -   Create an ngrok tunnel to your local web server (ngrok is bundled with ACE and doesn't need to be configured separately).
    -   Register your add-on's `atlassian-connect.json` (at `http://<temp-ngrok-url>.io/atlassian-connect.json`) with your host JIRA Cloud instance.
    -   Start watching for changes to your `atlassian-connect.json`. If the file is modified, `atlassian-connect-express` will re-register your add-on with the host.
3.  Finally, check that that your add-on is working correctly. Navigate to your JIRA Cloud instance and you'll see a **Hello World** link in the header. Click it and you should see a page like this:
    ![Alt text](../images/jiradev-helloworld.png)

{{% tip title="Congratulations!"%}}You've built a JIRA Cloud add-on using a framework.{{% /tip %}} 

---

## Next steps

If you'd like to keep learning about add-on development for JIRA Cloud, see the following pages:

-   Learn more about the other [frameworks and tools] for Atlassian Connect.
-   Try another tutorial. The [Adding a board configuration page] tutorial for JIRA Software will allow you to use the basic add-on that you've already built and extend it to add your own UI element.
-   Check out the [REST API] to see what is possible.


  [Getting started]: /cloud/jira/platform/getting-started
  [Atlassian Connect Express]: https://bitbucket.org/atlassian/atlassian-connect-express
  [frameworks and tools]: /cloud/jira/platform/frameworks-and-tools
  [Adding a board configuration page]: /cloud/jira/software/adding-a-board-configuration-page
  [REST API]: /cloud/jira/platform/jira-cloud-platform-rest-api
