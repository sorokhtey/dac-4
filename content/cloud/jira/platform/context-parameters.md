---
aliases:
    - /jiracloud/context-parameters.html
    - /jiracloud/context-parameters.md
    - /cloud/jira/platform/concepts/context-parameters.html
title: Context parameters
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2016-10-05"
---
{{< include path="content/cloud/connect/concepts/jira-context-parameters.snippet.md">}}
