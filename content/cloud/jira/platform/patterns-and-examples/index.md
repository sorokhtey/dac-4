---
Title: Patterns and examples
product: jiracloud
category: devguide
layout: patterns
---

{{< include path="content/cloud/connect/reference/jira-examples.snippet.md" >}}