---
title: About JIRA modules 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-39987281.html
- /jiracloud/jira-software-modules-39987281.md
confluence_id: 39987281
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987281
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987281
date: "2016-09-19"
---
{{< reuse-page path="content/cloud/jira/platform/about-jira-modules.md">}}

## JIRA Software modules

-   [Boards]

  [Boards]: /cloud/jira/software/boards
