---
title: JIRA cloud platform REST API scopes
platform: cloud
product: jswcloud
category: reference
subcategory: api
date: "2016-11-02"
---
# JIRA cloud platform REST API scopes

{{< include path="content/cloud/connect/reference/product-api-scopes.snippet.md" >}}

{{< include path="content/cloud/jira/platform/temp/jira-rest-api-scopes-reference.snippet.md" >}}