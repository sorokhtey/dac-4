---
title: Advanced Searching Using Cql 39985862
aliases:
    - /confcloud/advanced-searching-using-cql-39985862.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985862
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985862
confluence_id: 39985862
platform:
product:
category:
subcategory:
---
# Confluence Connect : Advanced Searching using CQL

The instructions on this page describe how to define and execute a search using the advanced search capabilities of the Confluence REST API. 

## What is an advanced search?

An advanced search allows you to use structured queries to search for content in Confluence. Your search results will take the same form as the Content model returned by the Content REST API.

When you perform an advanced search, you are using the Confluence Query Language (CQL).

A simple query in CQL (also known as a 'clause') consists of a *[field]* , followed by an *[operator]* , followed by one or more *values* or *[functions]* . For example, the following simple query will find all content in the "TEST" space. It uses uses the Space  *[field]* , the [EQUALS] *operator*, and the *value* `"TEST"`.)

``` bash
space = "TEST"
```

It is not possible to compare two [fields].

![(info)] CQL gives you some SQL-like syntax, such as the [ORDER BY] SQL keyword. However, CQL is not a database query language. For example, CQL does not have a `SELECT` statement.

-   [What is an advanced search?]
    -   [How to perform an advanced search]
    -   [Performing text searches]
    -   [Setting precedence of operators]
-   [Keyword reference]
-   [Operator reference]
-   [Field reference]

-   Function Reference

 

**Related topics:**

-   Performing text searches using CQL **
    **
-   Adding a field to CQL
-   CQL Field Module
-   CQL Function Module

### How to perform an advanced search

The Content API REST Resource now supports CQL as a query parameter to filter the list of returned content.

    http://myhost:8080/rest/api/content/search?cql=space=TEST

To perform an advanced search:

1.  Add your query using the [fields], [operators] and field values or [functions][1] as the value for the CQL query parameter.
2.  Execute a GET request on the resource, you can apply expansions and pagination as you would normally in the Confluence REST API.

### Performing text searches

You can use the [CONTAINS] operator to use Lucene's text-searching features when performing searches on these fields:

-   title
-   text
-   space.title

For details, please see the page on [Performing text searches][2].

### Setting precedence of operators

You can use parentheses in complex CQL statements to enforce the precedence of [operators].

For example, if you want to find all pages in the Developer space as well as all blog posts created by the the system administrator (bobsmith), you can use parentheses to enforce the precedence of the boolean operators in your query. For example:

``` bash
(type=page and Space=DEV) OR (creator=bobsmith and type=blogpost)
```

Note: if you do not use parentheses, the statement will be evaluated left to right.

You can also use parentheses to group clauses, so that you can apply the [NOT] operator to the group.

## Keyword reference

A keyword in CQL is a word or phrase that:

-   joins two or more clauses together to form a complex CQL query, or
-   alters the logic of one or more clauses, or
-   alters the logic of [operators], or
-   has an explicit definition in a CQL query, or
-   performs a specific function that alters the results of a CQL query.

**List of Keywords:**

-   [AND]
-   [OR]
-   [NOT]
-   [ORDER BY][3]

#### AND

Used to combine multiple clauses, allowing you to refine your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

###### Examples

-   Find all blogposts with the label "performance"

    ``` bash
    label = "performance" and type = "blogpost"
    ```

-   Find all pages created by jsmith in the DEV space

    ``` bash
    type = page and creator = jsmith and space = DEV
    ```

-   Find all content that mentions jsmith but was not created by jsmith

    ``` bash
    mention = jsmith and creator != jsmith
    ```

[^top of keywords] | [^^top of topic]

#### OR

Used to combine multiple clauses, allowing you to expand your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

(Note: also see [IN], which can be a more convenient way to search for multiple values of a field.)

###### Examples

-   Find all content in the IDEAS space or with the label idea

    ``` bash
    space = IDEAS or label = idea
    ```

-   Find all content last modified before the start of the year or with the label needs\_review

    ``` bash
    lastModified < startOfYear() or label = needs_review
    ```

[^top of keywords] | [^^top of topic]

#### NOT

Used to negate individual clauses or a complex CQL query (a query made up of more than one clause) using [parentheses], allowing you to refine your search.

(Note: also see [NOT EQUALS] ("!="), [DOES NOT CONTAIN] ("!~") and [NOT IN].)

###### Examples

-   Find all pages with the "cql" label that aren't in the dev space

    ``` bash
    label = cql and not space = dev 
    ```

[^top of keywords] | [^^top of topic]

#### ORDER BY

Used to specify the fields by whose values the search results will be sorted.

By default, the field's own sorting order will be used. You can override this by specifying ascending order ("`asc`") or descending order ("`desc`").

Not all fields support Ordering. Generally, ordering is not supported where a piece of content can have multiple values for a field, for instance ordering is not supported on labels.

###### Examples

-   Find content in the DEV space ordered by creation date

    ``` bash
    space = DEV order by created
    ```

-   Find content in the DEV space ordered by creation date with the newest first, then title

    ``` bash
    space = DEV order by created desc, title
    ```

-   Find pages created by jsmith ordered by space, then title

    ``` bash
    creator = jsmith order by space, title asc
    ```

[^top of keywords] | [^^top of topic]

## Operator reference

 An operator in CQL is one or more symbols or words which compares the value of a [field][4] on its left with one or more values (or <a href="http://advanced%20searching%20using%20cql/#functions" class="external-link">functions</a>) on its right, such that only true results are retrieved by the clause. Some operators may use the [NOT] keyword.

**List of Operators:**

-   [EQUALS: =]
-   [NOT EQUALS: !=]
-   [GREATER THAN: &gt;]
-   [GREATER THAN EQUALS: &gt;=]
-   [LESS THAN: &lt;]
-   [LESS THAN EQUALS: &lt;=]
-   [IN]
-   [NOT IN][5]
-   [CONTAINS: ~]
-   [DOES NOT CONTAIN: !~]

#### EQUALS: =

The "`=`" operator is used to search for content where the value of the specified field exactly matches the specified value. (Note: cannot be used with [text] fields; see the [CONTAINS] operator instead.)

To find content where the value of a specified field exactly matches *multiple* values, use multiple "`=`" statements with the [AND] operator.

###### Examples

-   Find all content that were created by jsmith:

    ``` bash
    creator = jsmith
    ```

-   Find all content that has the title "Advanced Searching"

    ``` bash
    title = "Advanced Searching"
    ```

[^top of operators][operators] | [^^top of topic]

#### NOT EQUALS: !=

The "`!=`" operator is used to search for content where the value of the specified field does not match the specified value. (Note: cannot be used with [text] fields; see the [DOES NOT MATCH] ("`!~`") operator instead.)

Note: typing `field != value` is the same as typing `NOT field = value`.

Currently a negative expression cannot be the first clause in a CQL statement

###### Examples

-   Find all content in the DEV space that was created by someone other than jsmith:

    ``` bash
    space = DEV and not creator = jsmith
    ```

    or:

    ``` bash
    space = DEV and creator != jsmith
    ```

-   Find all content that was created by me but doesn't mention me

    ``` bash
    creator = currentUser() and mention != currentUser()
    ```

[^top of operators][operators] | [^^top of topic]

#### GREATER THAN: &gt;

The "`>`" operator is used to search for content where the value of the specified field is greater than the specified value. Cannot be used with [text] fields.

Note that the "`>`" operator can only be used with fields which support range operators (e.g. date fields and numeric fields). To see a field's supported operators, check the individual [field] reference.

###### Examples

-   Find all content created in the last 4 weeks

    ``` bash
    created > now("-4w")
    ```

-   Find all attachments last modified since the start of the month

    ``` bash
    created > startOfMonth() and type = attachment
    ```

[^top of operators][operators] | [^^top of topic]

#### GREATER THAN EQUALS: &gt;=

The "`>=`" operator is used to search for content where the value of the specified field is greater than or equal to the specified value. Cannot be used with [text] fields.

Note that the "`>=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field] reference.

###### Examples

-   Find all content created on or after 31/12/2008:

    ``` bash
    created >= "2008/12/31"
    ```

[^top of operators][operators] | [^^top of topic]

#### LESS THAN: &lt;

The "`<`" operator is used to search for content where the value of the specified field is less than the specified value. Cannot be used with [text] fields.

Note that the "`<`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field] reference.

###### Examples

-   Find all pages lastModified before the start of the year

    ``` bash
    lastModified < startOfYear() and type = page
    ```

[^top of operators][operators] | [^^top of topic]

#### LESS THAN EQUALS: &lt;=

The "`<=`" operator is used to search for content where the value of the specified field is less than or equal to than the specified value. Cannot be used with [text] fields.

Note that the "`<=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field] reference.

###### Examples

-   Find blogposts created in the since the start of the fortnight

    ``` bash
    created >= startOfWeek("-1w") and type = blogpost
    ```

[^top of operators][operators] | [^^top of topic]

#### IN

The "`IN`" operator is used to search for content where the value of the specified field is one of multiple specified values. The values are specified as a comma-delimited list, surrounded by parentheses.

Using "`IN`" is equivalent to using multiple `EQUALS (=)` statements with the OR keyword, but is shorter and more convenient. That is, typing `creator IN (tom, jane, harry)` is the same as typing `creator = "tom" OR creator = "jane" OR creator = "harry"`.

###### Examples

-   Find all content that mentions either jsmith or jbrown or jjones:

    ``` bash
    mention in (jsmith,jbrown,jjones)
    ```

-   Find all content where the creator or contributor is either Jack or Jill:

    ``` bash
    creator in (Jack,Jill) or contributor in (Jack,Jill)
    ```

[^top of operators][operators] | [^^top of topic]

#### NOT IN

The "`NOT IN`" operator is used to search for content where the value of the specified field is not one of multiple specified values.

Using "`NOT IN`" is equivalent to using multiple `NOT_EQUALS (!=)` statements, but is shorter and more convenient. That is, typing `creator NOT IN (tom, jane, harry)` is the same as typing `creator != "tom" AND creator != "jane" AND creator != "harry"`.

###### Examples

-   Find all content where the creator is someone other than Jack, Jill or John:

    ``` bash
    space = DEV and creator not in (Jack,Jill,John)
    ```

[^top of operators][operators] | [^^top of topic]

#### CONTAINS: ~

The "`~`" operator is used to search for content where the value of the specified field matches the specified value (either an exact match or a "fuzzy" match -- see examples below). The "~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax].

###### Examples

-   Find all content where the title contains the word "win" (or simple derivatives of that word, such as "wins"):

    ``` bash
    title ~ win
    ```

-   Find all content where the title contains a [wild-card] match for the word "win":

    ``` bash
    title ~ "win*"
    ```

-   Find all content where the text contains the word "advanced" and the word "search":

    ``` bash
    text ~ "advanced search"
    ```

[^top of operators][operators] | [^^top of topic]

#### DOES NOT CONTAIN: !~

The "`!~`" operator is used to search for content where the value of the specified field is not a "fuzzy" match for the specified value. The "!~" operator can only be used with text fields, for example:

-   title
-   text

Note: when using the "`!~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax].

###### Examples

-   Find all content where the title does not contain the word "run" (or derivatives of that word, such as "running" or "ran"):

    ``` bash
    space = DEV and title !~ run
    ```

[^top of operators][operators] | [^^top of topic]

## Field reference

A field in CQL is a word that represents an indexed property of content in Confluence. In a clause, a field is followed by an [operator][6], which in turn is followed by one or more values (or [functions][1]). The operator compares the value of the field with one or more values or functions on the right, such that only true results are retrieved by the clause.

**List of Fields:**

-   [Ancestor]
-   [Content]
-   [Created]
-   [Creator]
-   [Contributor]
-   [Favourite, favorite]
-   [ID]
-   [Label]
-   [LastModified]
-   [Macro]
-   [Mention]
-   [Parent]
-   [Space]
-   [Text]
-   [Title]
-   [Type]
-   [Watcher]

#### Ancestor

Search for all pages that are descendants of a given ancestor page. This includes direct child pages and their descendents. It is more general than the [parent] field.

###### Syntax

``` bash
ancestor
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find all descendent pages with a given anscestor page

    ``` bash
    ancestor = 123
    ```

-   Find descendants of a group of ancestor pages

    ``` bash
    ancestor in (123, 456, 789)
    ```

[^top of fields][fields] | [^^top of topic]

#### Content

Search for content that have a given content ID. This is an alias of the [ID][7] field.

###### Syntax

``` bash
content
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None
###### Examples

-   Find content with a given content id

    ``` bash
    content = 123
    ```

-   Find content in a set of content ids

    ``` bash
    content in (123, 223, 323)
    ```

[^top of fields][fields] | [^^top of topic]

#### Created

Search for content that was created on, before or after a particular date (or date range).

Note: search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`
`"yyyy-MM-dd HH:mm"`
`"yyyy/MM/dd"`
`"yyyy-MM-dd"`

###### Syntax

``` bash
created
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [startOfMonth()]
-   [startOfWeek()]
-   [startOfYear()]

###### Examples

-   Find content created after the 1st September 2014

    ``` bash
    created > 2014/09/01
    ```

-   Find content created in the last 4 weeks

    ``` bash
    created >= now("-4w")
    ```

[^top of fields][fields] | [^^top of topic]

#### Creator

Search for content that was created by a particular user. You can search by the user's username.

###### Syntax

``` bash
creator
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content created by jsmith

    ``` bash
    created = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` bash
    created in (jsmith, bnguyen)
    ```

[^top of fields][fields] | [^^top of topic]

#### Contributor

Search for content that was created or edited by a particular user. You can search by the user's username.

###### Syntax

``` bash
contributor
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content created by jsmith

    ``` bash
    contributor = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` bash
    contributor in (jsmith, bnguyen)
    ```

[^top of fields][fields] | [^^top of topic]

#### Favourite, favorite

Search for content that was favourited by a particular user. You can search by the user's username.

Due to security restrictions you are only allowed to filter on the logged in user's favourites. This field is available in both British and American spellings.

###### Syntax

``` bash
favourite
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content that is favourited by the current user

    ``` bash
    favourite = currentUser()
    ```

-   Find content favourited by jsmith, where jsmith is also the logged in user

    ``` bash
    favourite = jsmith
    ```

[^top of fields][fields] | [^^top of topic]

#### ID

Search for content that have a given content ID.

###### Syntax

``` bash
id
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content with the id 123

    ``` bash
    id = 123
    ```

-   Find content in a set of content ids

    ``` bash
    id in (123, 223, 323)
    ```

[^top of fields][fields] | [^^top of topic]

#### Label

Search for content that has a particular label

###### Syntax

``` bash
label
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content that has the label finished

    ``` bash
    label = finished
    ```

-   Find content that doesn't have the label draft or review

    ``` bash
    label not in (draft, review)
    ```

[^top of fields][fields] | [^^top of topic]

#### LastModified

Search for content that was last modified on, before, or after a particular date (or date range).

The search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`
`"yyyy-MM-dd HH:mm"`
`"yyyy/MM/dd"`
`"yyyy-MM-dd"`

###### Syntax

``` bash
lastmodified
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [startOfMonth()]
-   [startOfWeek()]
-   [startOfYear()]

###### Examples

-   Find content that was last modified on 1st September 2014

    ``` bash
    lastmodified = 2014-09-01
    ```

-   Find content that was last modified before the start of the year

    ``` bash
    lastmodified < startOfYear()
    ```

-   Find content that was last modified on or after 1st September but before 9am on 3rd September 2014

    ``` bash
    lastmodified >= 2014-09-01 and lastmodified < "2014-09-03 09:00"
    ```

[^top of fields][fields] | [^^top of topic]

#### Macro

Search for content that has an instance of the macro with the given name in the body of the content

###### Syntax

``` bash
macro
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that has the JIRA issue macro

    ``` bash
    macro = jira
    ```

-   Find content that has Table of content macro or the widget macro

    ``` bash
    macro in (toc, widget)
    ```

[^top of fields][fields] | [^^top of topic]

#### Mention

Search for content that mentions a particular user. You can search by the user's username.

###### Syntax

``` bash
mention
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content that mentions jsmith or kjones

    ``` bash
    mention in (jsmith, kjones)
    ```

-   Find content that mentions jsmith

    ``` bash
    mention = jsmith
    ```

[^top of fields][fields] | [^^top of topic]

#### Parent

Search for child content of a particular parent page

###### Syntax

``` bash
parent
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

###### Examples

-   Find child pages of a parent page with ID 123

    ``` bash
    parent = 123
    ```

[^top of fields][fields] | [^^top of topic]

#### Space

Search for content that is in a particular Space. You can search by the space's key.

###### Syntax

``` bash
space
```

###### Field Type

SPACE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content in the development space or the QA space

    ``` bash
    space in (DEV, QA)
    ```

-   Find content in the development space

    ``` bash
    space = DEV
    ```

[^top of fields][fields] | [^^top of topic]

#### Text

This is a "master-field" that allows you to search for text across a number of other text fields. These are the same fields used by Confluence's search user interface.

-   <a href="http://developer.atlassian.com/#Title" class="external-link">Title</a>
-   Content body
-   <a href="http://developer.atlassian.com/#Labels" class="external-link">Labels</a>

Note: [Confluence text-search syntax] can be used with this field.

###### Syntax

``` bash
text
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that contains the word Confluence

    ``` bash
    text ~ Confluence
    ```

-   Find content in the development space

    ``` bash
    space = DEV
    ```

[^top of fields][fields] | [^^top of topic]

#### Title

Search for content by title, or with a title that contains particular text.

Note: [Confluence text-search syntax] can be used with this fields when used with the [CONTAINS][8] operator ("~", "!~")

###### Syntax

``` bash
title
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content with the title "Advanced Searching using CQL"

    ``` bash
    title = "Advanced Searching using CQL"
    ```

-   Find content that matches Searching CQL (i.e. a "fuzzy" match):

    ``` bash
    title ~ "Searching CQL"
    ```

[^top of fields][fields] | [^^top of topic]

#### Type

Search for content of a particular type. Supported content types are:

-   page
-   blogpost
-   comment
-   attachment

###### Syntax

``` bash
type
```

###### Field Type

TYPE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find blogposts or pages

    ``` bash
    type IN (blogpost, page)
    ```

-   Find attachments

    ``` bash
    type = attachment
    ```

[^top of fields][fields] | [^^top of topic]

#### Watcher

Search for content that a particular user is watching. You can search by the user's username.

###### Syntax

``` bash
watcher
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Search for content that you are watching:

    ``` bash
    watcher = currentUser()
    ```

-   Search for content that the user "jsmith" is watching:

    ``` bash
    watcher = "jsmith"
    ```

[^top of fields][fields] | [^^top of topic]

  [field]: #AdvancedSearchingusingCQL-field
  [operator]: #AdvancedSearchingusingCQL-operator
  [functions]: /confcloud/cql-function-reference-39985867.html
  [EQUALS]: #AdvancedSearchingusingCQL-EQUALS
  [fields]: #AdvancedSearchingusingCQL-fields
  [(info)]: /confcloud/images/icons/emoticons/information.png
  [ORDER BY]: #AdvancedSearchingusingCQL-ORDER_BY
  [What is an advanced search?]: #AdvancedSearchingusingCQL-Whatisanadvancedsearch?
  [How to perform an advanced search]: #AdvancedSearchingusingCQL-QueryHowtoperformanadvancedsearch
  [Performing text searches]: #AdvancedSearchingusingCQL-textPerformingtextsearches
  [Setting precedence of operators]: #AdvancedSearchingusingCQL-parenthesesSettingprecedenceofoperators
  [Keyword reference]: #AdvancedSearchingusingCQL-Keywordreference
  [Operator reference]: #AdvancedSearchingusingCQL-Operatorreference
  [Field reference]: #AdvancedSearchingusingCQL-Fieldreference
  [operators]: #AdvancedSearchingusingCQL-operators
  [1]: #AdvancedSearchingusingCQL-function
  [CONTAINS]: #AdvancedSearchingusingCQL-CONTAINS
  [2]: /confcloud/performing-text-searches-using-cql-39985876.html
  [NOT]: #AdvancedSearchingusingCQL-NOT
  [AND]: #AdvancedSearchingusingCQL-AND
  [OR]: #AdvancedSearchingusingCQL-OR
  [3]: #AdvancedSearchingusingCQL-ORDERBY
  [parentheses]: #AdvancedSearchingusingCQL-parentheses
  [^top of keywords]: #AdvancedSearchingusingCQL-keywords
  [^^top of topic]: #AdvancedSearchingusingCQL-top
  [IN]: #AdvancedSearchingusingCQL-IN
  [NOT EQUALS]: #AdvancedSearchingusingCQL-NOT_EQUALS
  [DOES NOT CONTAIN]: #AdvancedSearchingusingCQL-DOES_NOT_CONTAIN
  [NOT IN]: #AdvancedSearchingusingCQL-NOT_IN
  [4]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-field
  [EQUALS: =]: #AdvancedSearchingusingCQL-EQUALS:=
  [NOT EQUALS: !=]: #AdvancedSearchingusingCQL-NOTEQUALS:!=
  [GREATER THAN: &gt;]: #AdvancedSearchingusingCQL-GREATERTHAN:%3E
  [GREATER THAN EQUALS: &gt;=]: #AdvancedSearchingusingCQL-GREATERTHANEQUALS:%3E=
  [LESS THAN: &lt;]: #AdvancedSearchingusingCQL-LESSTHAN:%3C
  [LESS THAN EQUALS: &lt;=]: #AdvancedSearchingusingCQL-LESSTHANEQUALS:%3C=
  [5]: #AdvancedSearchingusingCQL-NOTIN
  [CONTAINS: ~]: #AdvancedSearchingusingCQL-CONTAINS:~
  [DOES NOT CONTAIN: !~]: #AdvancedSearchingusingCQL-DOESNOTCONTAIN:!~
  [text]: #AdvancedSearchingusingCQL-text
  [DOES NOT MATCH]: #AdvancedSearchingusingCQL-DOES_NOT_MATCH
  [Confluence text-search syntax]: https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL
  [wild-card]: /confcloud/cql-operators-reference-39985874.html
  [6]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operator
  [Ancestor]: #AdvancedSearchingusingCQL-ancestorAncestorAncestor
  [Content]: #AdvancedSearchingusingCQL-contentContentContent
  [Created]: #AdvancedSearchingusingCQL-createdCreatedCreated
  [Creator]: #AdvancedSearchingusingCQL-creatorcreatorCreator
  [Contributor]: #AdvancedSearchingusingCQL-contributorcontributorContributor
  [Favourite, favorite]: #AdvancedSearchingusingCQL-favouriteFavouriteFavourite,favorite
  [ID]: #AdvancedSearchingusingCQL-idIDID
  [Label]: #AdvancedSearchingusingCQL-labelLabelLabel
  [LastModified]: #AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified
  [Macro]: #AdvancedSearchingusingCQL-macroMacroMacro
  [Mention]: #AdvancedSearchingusingCQL-mentionMentionMention
  [Parent]: #AdvancedSearchingusingCQL-parentParentParent
  [Space]: #AdvancedSearchingusingCQL-spaceSpaceSpace
  [Text]: #AdvancedSearchingusingCQL-textTextText
  [Title]: #AdvancedSearchingusingCQL-titleTitleTitle
  [Type]: #AdvancedSearchingusingCQL-titleTitleType
  [Watcher]: #AdvancedSearchingusingCQL-Watcher
  [parent]: https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#parent
  [7]: https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#ID
  [endOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay
  [endOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth
  [endOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek
  [endOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear
  [startOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay
  [startOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth
  [startOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek
  [startOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear
  [currentUser()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser()
  [8]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-CONTAINS:~

