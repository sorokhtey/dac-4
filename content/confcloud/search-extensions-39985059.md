---
title: Search Extensions 39985059
aliases:
    - /confcloud/search-extensions-39985059.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985059
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985059
confluence_id: 39985059
platform:
product:
category:
subcategory:
---
# Confluence Connect : Search extensions

<img src="/confcloud/attachments/39985059/44063097.png" width="434" />

## What are Search extensions?

A Search extension assigns custom properties to Confluence content and enables search filtering using those properties. In the REST API, they allow you to write [Confluence Query Language] (CQL) queries on content using your custom properties as terms.

## What can I build with Search extensions?

Search extensions allow you to build add-ons with powerful filtering capabilities for content.

For example, search extensions could be used to create:

-   An add-on that ranks each page in Confluence and allows the user to search for only highly ranked pages.
-   An add-on that intelligently categorises pages and allows search filtering on this category.

### Extend the search API with content properties

By storing properties against a piece of content and [declaring them as indexable] by the Confluence search, you can write CQL queries that use them as terms.

For example, if you decide to save a 'last viewed date' on every page, you can then use CQL to query the Content API for all pages viewed in the past week, month, or year.

This might look like:

``` bash
type = page and lastViewedDate > now(-7d)
```

### Custom search and macro filters

Filtering content by querying the REST API as a developer is useful, but we should also make filtering possible for end users. With [UI support], you can use your content properties to add powerful search filters to macros that aggregate and display content, like the <a href="https://confluence.atlassian.com/doc/content-by-label-macro-145566.html" class="external-link">content by label macro</a>. As well as macros, your filters will also appear in the search screen, allowing users to refine their search queries.

 

<img src="/confcloud/attachments/39985059/44063099.png" class="image-center" width="554" height="400" />

 

Filter your search results by a custom property set on the content

## How do I build search extensions?

Content properties are key-value pairs stored on a page or blog post in Confluence. To get started with search extensions, you'll need to use the [REST API] to store content properties (which must be valid JSON) against a piece of content.

To [integrate with search][declaring them as indexable], you'll need to define some [extractions] to declare the fields and nested data you want to be indexable by Confluence. You'll also want to define an [alias] for simpler CQL querying, and [UI support] for your fields to be filterable by users on the search screen. You can declare both of these in your atlassian-connect.json file.

## What other patterns might be helpful?

### Page extensions

[Page extensions] may also provide another useful way of surfacing metadata related to a page.

## Let's do this!

Get going by following our [getting started tutorial] and referring to related documentation listed on the right hand side of this page. 

### Related Add-on categories

-   Integrations

### Related Connect modules

-   [Content Property Index Extractions][extractions]
-   [Content Property Aliases][alias]
-   [Content Property UI Support][UI support]

### Related REST APIs

-   [Content properties in the REST API]
-   [Advanced searching using CQL]

### Related documentation

-   <a href="https://confluence.atlassian.com/display/ConfCloud/Search" class="external-link">Confluence Search</a>
-   <a href="https://confluence.atlassian.com/display/ConfCloud/Content+by+Label+Macro" class="external-link">Content by Label Macro</a>
-   [Powerful new search and property storage APIs in Confluence][Confluence Query Language]
-   [Confluence Connect integrations with CQL]

  [Confluence Query Language]: https://developer.atlassian.com/blog/2015/02/confluence-5-7-apis/
  [declaring them as indexable]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/content-property-index-key-configuration.html
  [UI support]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/user-interface-support.html
  [REST API]: https://developer.atlassian.com/display/CONFDEV/Content+Properties+in+the+REST+API
  [extractions]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/content-property-index-extraction-configuration.html
  [alias]: https://developer.atlassian.com/static/connect/docs/latest/modules/fragment/content-property-index-extraction-configuration.html#alias
  [Page extensions]: /confcloud/page-extensions-39985058.html
  [getting started tutorial]: /confcloud/search-extensions-39985059.html
  [Content properties in the REST API]: https://developer.atlassian.com/confdev/confluence-rest-api/content-properties-in-the-rest-api
  [Advanced searching using CQL]: https://developer.atlassian.com/confdev/confluence-rest-api/advanced-searching-using-cql
  [Confluence Connect integrations with CQL]: https://developer.atlassian.com/blog/2016/01/confluence-connect-integrations-with-CQL/

