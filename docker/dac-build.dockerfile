FROM ubuntu:xenial

# Install SASS, Node/NPM, and misc utils
RUN apt-get update && \
    apt-get install -y python3 virtualenv \
                       libsass0 nodejs npm  \
                       curl git && \
    apt-get clean

RUN ln -s /usr/bin/nodejs /usr/bin/node


# Install Hugo
RUN curl -L "https://bitbucket.org/atlassian/dac-hugo-custom-builds/raw/master/hugo_linux_amd64.tar.gz" | \
    tar xz -C "/usr/local/bin/"