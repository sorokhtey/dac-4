FROM ubuntu:xenial

RUN apt-get update && \
    apt-get install -y git make curl && \
    apt-get clean

# Install Golang
RUN mkdir go && curl -L "https://storage.googleapis.com/golang/go1.7.3.linux-amd64.tar.gz" | \
    tar xz -C "/usr/local"

# Setup Go environment
ENV GOPATH /go
ENV PATH /usr/local/go/bin:$PATH

# Get Hugo sources
# Checkout the branch we want (v0.17 at this time)
# Sync the dependencies at that version
RUN go get -u -t github.com/spf13/hugo/... && \
    go get -u github.com/kardianos/govendor && \
    cd $GOPATH/src/github.com/spf13/hugo && \
    git checkout v0.17 && \
    /go/bin/govendor sync
