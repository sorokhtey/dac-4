import os
from pyquery import PyQuery

import dac

from bs4 import BeautifulSoup

test_resources = os.path.join(os.path.abspath(__file__), 'test_resources')

sample_html = """
    <html>
    <head>
    <title>test data</title>
    </head>
    <body>
    <h1 id="title-heading"><span class="junk">big title</span></h1>

    <div id="main-content">
        <li class="first">
            <span><a href="index.html">JIRA Cloud</a></span>
        </li>
        <li>
            <span><a href="JIRA-Cloud-for-Developers_39375886.html">JIRA Cloud for Developers</a></span>
        </li>
        <p>This is some compelling developer-facing content.</p>
        <div class="conf-macro output-inline" id="whatevs">something</div>
    </div>

    <div class="aui-page-panel-inner" id="feedback-block">
        Don't need this stuff!
    </div>
    </body>
    </html>
"""


def test_link_nicer():
    t = dac.Transformer
    nicer = t.confluence_link_nicer
    assert nicer("") == ""
    already_good = "/jiracloud/jira-platform-tutorials-39987044.html"
    assert nicer(already_good) == already_good
    link = "JIRA-Cloud-for-Developers_39375886.html"
    expected = "jira-cloud-for-developers-39375886.html"
    assert nicer(link) == expected


def test_simple_transform_links_in_lines():
    os.makedirs("tmp", exist_ok=True)
    html_file = "tmp/JIRA-Cloud-for-Developers_39375886.html"
    with open(html_file, "w") as mock_file:
        print("nothing to see here", file=mock_file)

    t = dac.Transformer("tmp")
    lines = ''.join(['<a href="JIRA-Cloud-for-Developers_39375886.html">',
                     '<a href="JIRA-Service-Desk-Cloud-development_39981106.html">'])
    transformed = t.transform_links(lines)
    os.remove(html_file)
    assert transformed.count("JIRA-Cloud-for-Developers_39375886.html") == 0
    assert transformed.count("jira-cloud-for-developers-39375886.html") == 1
    # because these files do not exist, we don't want to transform these links
    assert transformed.count("JIRA-Service-Desk-Cloud-development_39981106.html") == 1
    assert transformed.count("jira-service-desk-cloud-development-39981106.html") == 0


def test_clean_dom():
    transformed = dac.Transformer("tmp").clean_dom(sample_html)
    assert transformed.count("Don't need this stuff!") == 0
    assert transformed.count("body") == 0
    assert transformed.count("This is some compelling developer-facing content.") == 1
    assert transformed.count('<div id="main-content">') == 1
    assert transformed.count('<h1 id="title-heading">big title</h1>') == 1
    expected = """<h1 id="title-heading">big title</h1>
    <div id="main-content">
        <li class="first">
            <span><a href="index.html">JIRA Cloud</a></span>
        </li>
        <li>
            <span><a href="JIRA-Cloud-for-Developers_39375886.html">JIRA Cloud for Developers</a></span>
        </li>
        <p>This is some compelling developer-facing content.</p>
        <div class="conf-macro output-inline" id="whatevs">something</div>
    </div>
    """
    assert_html_same(expected, transformed)


def test_remove_unwanted_classes():
    html = """
    <div class="emoticon stuff conf-macro">
        <p class="dance output-inline">
            Developers, developers, developers, <b class="confluence-content-image-border">developers</b>!
        </p>
    </div>
    """
    actual = dac.Transformer.remove_unwanted_classes(html)
    d = PyQuery(actual)
    assert not d.is_(".output-inline")
    assert not d.is_(".confluence-content-image-border")
    assert not d.is_('.output-inline')
    assert d('.dance b').text() == "developers"
    assert d.is_('.stuff .dance')


def test_smoosh_brush():
    brush_class = '<pre class="brush: js; gutter: false; theme: Confluence">code here</pre>'
    expected = '<pre class="brush:js; gutter:false; theme:Confluence">code here</pre>'
    t = dac.Transformer("tmp")
    actual = t.preprocess_code_blocks(brush_class)
    assert expected == actual


def test_reduce_code_block_to_language():
    pre = """
    <div class="code panel pdl">
        <div class="codeContent panelContent pdl">
            <pre data-syntaxhighlighter-params="brush:js; gutter:false; theme:Confluence">
                code here, yo
            </pre>
        </div>
    </div>
    """
    actual = dac.Transformer.reduce_code_block_to_language(pre)
    expected = """
    <div class="code panel pdl">
        <div class="codeContent panelContent pdl">
            <pre class="javascript" data-syntaxhighlighter-params="brush:js; gutter:false; theme:Confluence">
                code here, yo
            </pre>
        </div>
    </div>
    """
    assert_html_same(expected, actual)


def test_clean_html_fragments():
    cl = dac.Transformer("tmp").clean_dom
    assert cl("no desired content") == ''
    assert cl('<div id="main-content">hello</div>') == '<div id="main-content">hello</div>'
    div_with_classes = '<div class="conf-macro output-inline" id="main-content">yo</div>'
    assert cl(div_with_classes) == div_with_classes


def test_titlefy():
    basename = "the-rest-api-is-surely-a-jira-api-for-the-ui-yo-12345"
    assert dac.Transformer.titlefy(basename) == "The REST API is Surely a JIRA API for the UI Yo 12345"


def assert_html_same(expected, actual):
    expected_soup = BeautifulSoup(expected.strip(), 'html.parser').prettify()
    actual_soup = BeautifulSoup(actual.strip(), 'html.parser').prettify()
    assert expected_soup == actual_soup


def test_clean_spans():
    html_with_spans = """
    <div><p>This is legit.</p>
        <p><span class="this doll has voodoo properties"><b>The</b> Im<b>balm</b>er</span>. Hair are our aerials.</p>
        <span class="scrubbers">The carrot</span>.
    </div>
    """

    actual = dac.Transformer.clean_spans(html_with_spans)
    assert actual.count('<span') == 0
    expected = """
        <div><p>This is legit.</p>
            <p><b>The</b> Im<b>balm</b>er. Hair are our aerials.</p>
            The carrot.
        </div>
    """
    assert_html_same(expected, actual)

def test_rewrite_image_links_2():
    wrong_image_html = """<img class="image-center" data-base-url="https://developer.atlassian.com" data-image-src="attachments/39375836/39984493.png" data-linked-resource-container-id="39375836" data-linked-resource-container-version="38" data-linked-resource-content-type="image/png" data-linked-resource-default-alias="image2016-4-19 15:41:47.png" data-linked-resource-id="39984493" data-linked-resource-type="attachment" data-linked-resource-version="1" data-unresolved-comment-count="0" src="attachments/39375836/39984493.png"/>"""
    actual = dac.Transformer('tmp', "http://developer.atlassian.com", "base").rewrite_image_links_with_alias(wrong_image_html)
    expected = """<img class="image-center" data-base-url="https://developer.atlassian.com" data-image-src="images/image2016-4-19-15:41:47.png" data-linked-resource-container-id="39375836" data-linked-resource-container-version="38" data-linked-resource-content-type="image/png" data-linked-resource-default-alias="image2016-4-19 15:41:47.png" data-linked-resource-id="39984493" data-linked-resource-type="attachment" data-linked-resource-version="1" data-unresolved-comment-count="0" src="images/image2016-4-19-15:41:47.png"/>"""
    assert_html_same(expected, actual)

def test_rewrite_image_links():
    wrong_image_html = """
    <div id="whatevs">
        <img data-linked-resource-default-alias="Monty.gif" src="monty.gif">
        <p>I have of late, but wherefore I know not, lost all my mirth.</p>
        <img data-linked-resource-default-alias="Screenshot 12_12.png" src="camberwell-carrot.png" />
    </div>
    """
    actual = dac.Transformer('tmp', "http://developer.atlassian.com", "base").rewrite_image_links_with_alias(wrong_image_html)
    expected = """
    <div id="whatevs">
        <img data-linked-resource-default-alias="Monty.gif" src="images/monty.gif">
        <p>I have of late, but wherefore I know not, lost all my mirth.</p>
        <img data-linked-resource-default-alias="Screenshot 12_12.png" src="images/screenshot-12-12.png" />
    </div>
    """
    assert_html_same(expected, actual)

def test_rewrite_image_links_3():
    wrong_image_html = """<img class="emoticon emoticon-tick" src="images/icons/emoticons/check.png" data-emoticon-name="tick" alt="(tick)"/>"""
    actual = dac.Transformer('tmp', "http://developer.atlassian.com", "base").rewrite_image_links_simple(wrong_image_html)
    expected = """<img class="emoticon emoticon-tick" src="/base/images/icons/emoticons/check.png" data-emoticon-name="tick" alt="(tick)"/>"""
    assert_html_same(expected, actual)
