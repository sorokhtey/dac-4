<VirtualHost *:80>

  ServerAdmin webmaster@atlassian.com
  ServerName developer.stg.internal.atlassian.com
  ServerAlias developers.stg.internal.atlassian.com dev.stg.internal.atlassian.com

  # These RedirectMatch directives are required because otherwise the Alias's below do not work correctly.
  # Without these if a user visits /design without a trailing slash then Apache automatically redirects them to
  # http://developer-fe.stg.internal.atlassian.com/design/ <- note the internal hostname. This is the hostname
  # that NginX is proxypassing to. These rules allow us to do our own redirect and fix this broken behaviour.
  RedirectMatch ^/static$ http://developer.stg.internal.atlassian.com/static/
  RedirectMatch ^/stash$ http://developer.stg.internal.atlassian.com/stash/

  # Redirect for Design ADM-50794
  # See /etc/apache2/sites-enabled/design.stg.internal.atlassian.com.conf
  RedirectMatch ^/design/* http://design.stg.internal.atlassian.com

  RewriteEngine on
  # Redirect old urls without versioning
  RewriteRule ^/static/connect/docs/(404\.html|assets/|concepts/|developing/|guides/|index.html|javascript/|modules/|release-notes/|resources/|rest\-apis/|scopes/)(.*) /static/connect/docs/latest/$1$2 [R,L]
  RewriteRule ^/static/connect/docs/?$ /static/connect/docs/latest/ [R,L]
  RewriteRule ^/connect$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Atlassian\+Connect$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Introduction\+to\+Atlassian\+Connect$ /static/connect/docs/latest/guides/introduction.html [R,L]
  RewriteRule ^/display/AC/Getting\+Started$ /static/connect/docs/latest/guides/getting-started.html [R,L]
  RewriteRule ^/display/AC/Hello\+World$ /static/connect/docs/latest/guides/getting-started.html [R,L]
  RewriteRule ^/display/AC/Add-on\+Descriptors$ /static/connect/docs/latest/modules/ [R,L]
  RewriteRule ^/display/AC/Pages$ /static/connect/docs/latest/modules/common/page.html [R,L]
  RewriteRule ^/display/AC/Webhooks$ /static/connect/docs/latest/modules/common/webhook.html [R,L]
  RewriteRule ^/display/AC/Development\+Loop$ /static/connect/docs/latest/developing/developing-locally.html [R,L]
  RewriteRule ^/display/AC/Installing+an\+Add-on$ /static/connect/docs/latest/developing/installing-in-ondemand.html [R,L]
  RewriteRule ^/display/AC/Add-on\+Design\+Considerations$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Licensing$ /static/connect/docs/latest/concepts/licensing.html [R,L]
  RewriteRule ^/display/AC/Listing\+Private+Add-ons$ /static/connect/docs/latest/developing/installing-in-ondemand.html [R,L]
  RewriteRule ^/display/AC/Security$ /static/connect/docs/latest/concepts/security.html [R,L]
  RewriteRule ^/display/AC/Scopes$ /static/connect/docs/latest/scopes/scopes.html [R,L]
  RewriteRule ^/display/AC/Authenticating\+with\+OAuth$ /static/connect/docs/latest/concepts/authentication.html [R,L]
  RewriteRule ^/display/AC/Permission\+Conditions$ /static/connect/docs/latest/concepts/authentication.html [R,L]
  RewriteRule ^/display/AC/Managing\+Access\+Tokens$ /static/connect/docs/latest/developing/cloud-installation.html [R,L]
  RewriteRule ^/display/AC/Selling\+on\+the\+Atlassian\+Marketplace$ /static/connect/docs/latest/developing/selling-on-marketplace.html [R,L]
  RewriteRule ^/display/AC/Developer\+Resources$ /static/connect/docs/latest/index.html [R,L]
  RewriteRule ^/display/AC/Interactive\+Descriptor\+Guide$ /static/connect/docs/latest/modules/ [R,L]
  RewriteRule ^/display/AC/Sample\+Add-ons$ /static/connect/docs/latest/resources/samples.html [R,L]
  RewriteRule ^/display/AC/REST\+API\+Browser$ /static/connect/docs/latest/rest-apis/product-api-browser.html [R,L]
  RewriteRule ^/display/AC/Atlassian\+Connect\+FAQs$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/Business\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/General\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/display/AC/Technical\+FAQ$ /static/connect/docs/latest/resources/faqs.html [R,L]
  RewriteRule ^/static/connect/?$ /static/connect/docs/latest/ [R,L]
  RewriteRule ^/static/connect/docs/latest/developing/installing-in-ondemand.html$ /static/connect/docs/latest/developing/cloud-installation.html [R=301,L,NC]
  RewriteRule ^/static/connect-versions.json$ /static/connect/commands/connect-versions.json [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/(general|admin|configure|user-profile)-page.html$ /static/connect/docs/latest/modules/common/page.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-item.html$ /static/connect/docs/latest/modules/common/web-item.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-panel.html$ /static/connect/docs/latest/modules/common/web-panel.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/web-section.html$ /static/connect/docs/latest/modules/common/web-section.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/(jira|confluence)/webhook.html$ /static/connect/docs/latest/modules/common/webhook.html [R=301,L,NC]
  RewriteRule ^/static/connect/docs/latest/modules/jira/(issue|project|user-profile)-tab-panel.html$ /static/connect/docs/latest/modules/jira/tab-panel.html [R=301,L,NC]
  RewriteRule ^/static/$ /index.html [R=301,L,NC]
  RewriteRule ^/static/index.html$ /index.html [R=301,L,NC]
  RewriteRule ^/bitbucket/server/?$ /bitbucket/server/docs/latest/ [R,L]

  # PENG-4221
  RewriteRule ^/service-desk/*$ /display/jiracloud/JIRA+Service+Desk+Cloud+development [R=301,L]

  <Directory ~ "/*/static/connect/docs">
    ErrorDocument 404 /static/connect/docs/latest/404.html
  </Directory>

  SSLProxyEngine On

  # PENG-5562
  ProxyPassMatch /bitbucket/api/2/reference/js/(.*) https://bbc-api-docs.internal.useast.atlassian.io/js/$1
  ProxyPassMatch  /bitbucket/api/2/reference/images/(.*) https://bbc-api-docs.internal.useast.atlassian.io/images/$1
  ProxyPassMatch /bitbucket/api/2/reference/(.*) https://bbc-api-docs.internal.useast.atlassian.io/docs/bitbucket/2/resource/$1

  # PENG-5562: Redirect to page with trailing slash
  RewriteRule ^(/bitbucket/api/2/reference)$ $1/ [R=301,L]

  #PENG-4405
  ProxyPassMatch /market/api/2/reference/(.*) https://mpac-api-docs.internal.useast.staging.atlassian.io/docs/marketplace/2/resource/$1
  
  Alias /static /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static
  Alias /design /opt/j2ee/domains/atlassian.com/developer-staging/static-content/design
  Alias /blog /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/blog
  Alias /articles /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/articles
  Alias /stash  /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/stash
  Alias /bitbucket/server /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/bitbucket-server
  Alias /bitbucket /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/bitbucket
  Alias /opensource /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/opensource
  Alias /newsletter /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/newsletter

  # Redirect for ADM-46392
  Redirect /connect https://developer.stg.internal.atlassian.com/display/AC/

  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" \"%{atl_cid}C\" \"%{atl_prds}C\""  combined-cookie

  ErrorLog /var/log/apache2/developer.stg.internal.atlassian.com-error.log
  CustomLog /var/log/apache2/developer.stg.internal.atlassian.com-access.log combined-cookie

  # Need this otherwise the above Alias doesn't work.
  ProxyPass /bitbucket/server !
  ProxyPass /bitbucket !
  ProxyPass /static !
  ProxyPass /design !
  ProxyPass /blog !
  ProxyPass /articles !
  ProxyPass /stash !
  ProxyPass /connect !
  ProxyPass /getting-started !
  ProxyPass /nodejs !
  ProxyPass /api-reference !
  ProxyPass /opensource !
  ProxyPass /newsletter !

  # ADM-50127
  ProxyPass /index.html !
  ProxyPass /robots.txt !
  ProxyPass /help !
  ProxyPass /search !
  ProxyPass /js !
  ProxyPass /css !
  ProxyPass /.htaccess !
  ProxyPass /imgs !
  ProxyPass /fancybox !
  ProxyPass /video !

  Alias /index.html /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/index.html
  Alias /robots.txt /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/robots.txt
  Alias /help /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/help.html
  Alias /search /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/search.html
  Alias /js /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/js
  Alias /css /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/css
  Alias /.htaccess /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/.htaccess
  Alias /imgs /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/imgs
  Alias /video /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/video
  Alias /fancybox /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/fancybox
  Alias /getting-started /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/getting-started.html
  Alias /nodejs /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/nodejs.html
  Alias /api-reference /opt/j2ee/domains/atlassian.com/developer-staging/static-content/static/dac-homepage/current/api-reference.html

  RewriteRule ^/$ http://developer.stg.internal.atlassian.com/index.html

  ProxyPass / http://127.0.0.1:8081/
  ProxyPassReverse / http://127.0.0.1:8081/

</VirtualHost>