#!/bin/bash

cd ${GOPATH}/src/github.com/spf13/hugo
cp /data/micros/hugo/patches/0001-ATLASSIAN-missing-baseURL-gives-warning.patch .
git apply 0001-ATLASSIAN-missing-baseURL-gives-warning.patch

echo "Building hugo for ${GOOS}_${GOARCH}"
make

mv hugo /data/hugo_${GOOS}_${GOARCH}
