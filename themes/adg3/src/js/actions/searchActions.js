import fetch from 'isomorphic-fetch'

const googleCSEEndpoint = 'https://www.googleapis.com/customsearch/v1'
const searchEngineId = '011074820825930543885:c60npogu07w'
const apiKey = process.env.GOOGLE_CSE_API_KEY

export const REQUEST_SEARCH_RESULTS = 'REQUEST_SEARCH_RESULTS'
export const RECEIVE_SEARCH_RESULTS = 'RECEIVE_SEARCH_RESULTS'
export const CHANGE_QUERY_STRING = 'CHANGE_QUERY_STRING'
export const CHANGE_PAGINATION = 'CHANGE_PAGINATION'
export const CHANGE_SEARCH_LABEL = 'CHANGE_SEARCH_LABEL'

const requestSearchResults = (queryString) => ({
  type: REQUEST_SEARCH_RESULTS,
  queryString
})

const receiveSearchResults = (queryString, results, searchInformation, facets) => ({
  type: RECEIVE_SEARCH_RESULTS,
  queryString,
  results,
  searchInformation,
  facets
})

export const changeSearchQuery = (queryString) => ({
  type: CHANGE_QUERY_STRING,
  queryString
})

export const changePagination = (pageNumber) => ({
  type: CHANGE_PAGINATION,
  pageNumber
})

export const changeSearchLabel = (searchLabel) => ({
  type: CHANGE_SEARCH_LABEL,
  searchLabel
})

export const fetchSearchResults = () => (dispatch, getState) => {
  const {queryString, pageNumber, searchLabel} = getState().search
  if (!queryString) return
  const startIndex = pageNumber ? (pageNumber * 10) - 9 : '1'

  dispatch(requestSearchResults(queryString))

  const params = {
    start: startIndex,
    cx: searchEngineId,
    key: apiKey
  }
  const query = (searchLabel && searchLabel !== 'all')
    ? encodeURIComponent(queryString) + '+' + encodeURIComponent(searchLabel)
    : encodeURIComponent(queryString)
  const url = `${googleCSEEndpoint}?q=${query}&${jQuery.param(params)}`

  return fetch(url)
    .then(function(response) {
      if (response.status >= 400) {
        throw new Error("URL Does not exist.")
      }
      return response.text()
    })
    .then(function(json) {
      const response = JSON.parse(json)
      const {items, searchInformation, context} = response
      dispatch(receiveSearchResults(queryString, items, searchInformation, context && context.facets))
    })
    .catch(function(err) {
      console.log('Fetch returned an error:' + err)
      dispatch(receiveSearchResults(queryString))
    })
}
