import React, { Component, PropTypes } from 'react'
import Button from './Button'

class SearchBox extends Component {
  constructor(props) {
    super(props)
    this.state = {inputValue: ''}
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  componentWillReceiveProps(nextProps) {
    const {queryString} = nextProps
    if (queryString !== this.state.inputValue) {
      this.setState({inputValue: queryString})
    }
  }
  handleChange(e) {
    this.setState({inputValue: e.target.value})
  }
  handleSubmit(e) {
    e.preventDefault()
    this.props.onSearch(this.state.inputValue)
  }
  render() {
    return (
      <div className="row">
        <form className="column" onSubmit={this.handleSubmit}>
          <h1>
            <input className="search-input float-left small-10 medium-11" value={this.state.inputValue} onChange={this.handleChange} />
            <button type="submit" className="float-right">
              <span className="aui-icon aui-icon-small aui-iconfont-search" data-unicode="UTF+f194"></span>
            </button>
          </h1>
        </form>
      </div>
    );
  }
}

SearchBox.propTypes = {
  queryString: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired
}

export default SearchBox