import React, {Component, PropTypes} from "react"

const SearchResultItem = ({ title, snippet, link }) => (
  <div>
    <h4><a target="_blank" href={link}>{title}</a></h4>
    <p>{snippet}</p>
    <p className="subheader">{link}</p>
    <hr/>
  </div>
)

SearchResultItem.propTypes = {
  title: PropTypes.string.isRequired,
  snippet: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
}

export default SearchResultItem