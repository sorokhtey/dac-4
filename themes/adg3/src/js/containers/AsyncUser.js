import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchAIDProfile, loginAIDUser } from '../actions'
import Button from '../components/Button'

class AsyncUser extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { dispatch, profile } = this.props
    dispatch(fetchAIDProfile())
  }

  render() {
    const { profile, authenticated, isFetching, lastUpdated, onClickLogin } = this.props
    var avatarUrl = "#"
    if (authenticated) {
      avatarUrl = "https://avatar-cdn.dev.internal.atlassian.com" + "/" + profile.profile.emailHash + "?s=200"
    }
    return (
      <div>
        {isFetching && !authenticated &&
          <h2>Loading...</h2>
        }
        {authenticated && profile.profile != undefined &&
          <ul style={{ opacity: isFetching ? 0.5 : 1 }}>
            <li>Username: {profile.profile.username}</li>
            <li>Email: {profile.profile.email}</li>
            <li>Title: {profile.profile.title}</li>
            <img src = {avatarUrl} />
          </ul>
        }
        {authenticated && lastUpdated &&
          <div>
            Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
            {' '}
          </div>
        }
        {!authenticated && !isFetching &&
          <div>
            <h2>Signup or...</h2>
            <Button text='Login' onClick={onClickLogin} />
          </div>
        }
      </div>
    )
  }
}

AsyncUser.propTypes = {
  profile: PropTypes.any.isRequired,
  authenticated: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  lastUpdated: PropTypes.number,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  const { profile } = state
  const {
    isFetching,
    authenticated,
    lastUpdated
    } = profile || {
        isFetching: true,
        authenticated: false,
        profile: {}
    }

  return {
    profile,
    authenticated,
    isFetching,
    lastUpdated
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClickLogin: () => {
      dispatch(loginAIDUser())
    },
    dispatch: dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AsyncUser)
