import React, {Component, PropTypes} from "react"
import {connect} from "react-redux"
import {changeSearchQuery, changePagination, changeSearchLabel, fetchSearchResults} from "../actions/searchActions.js"
import SearchBox from '../components/SearchBox'
import SearchResults from '../components/SearchResults'
import Pagination from '../components/Pagination'
import SearchLabels from '../components/SearchLabels'


class Search extends Component {
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(changeSearchQuery(location.search ? decodeURIComponent(location.search.split('?q=')[1]) : ''))
    dispatch(fetchSearchResults())
  }
  render() {
    const { isFetching, onPaginationClick, searchInformation, searchFacets, pageNumber} = this.props
    return (
      <div>
        <SearchBox {...this.props} />
        <div className='row'>
          {searchFacets != undefined && searchFacets.length > 0 &&
            <div className='hide-for-small-only medium-3 columns'>
              <SearchLabels {...this.props} />
            </div>
          }
          <div className='small-12 medium-9 columns'>
            <SearchResults {...this.props} />
          </div>
        </div>
        <div className="row">
          <div className="column medium-10 float-right">
            {!isFetching && searchInformation && searchInformation.totalResults > 10 &&
              <Pagination totalItemsCount={parseInt(searchInformation.totalResults)} activePage={pageNumber} onChange={onPaginationClick}/>
            }
          </div>
        </div>
      </div>
    )
  }
}

Search.propTypes = {
  searchResults: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  queryString: PropTypes.string,
  searchLabel: PropTypes.string,
  pageNumber: PropTypes.number,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  return state.search
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSearch: (queryString) => {
      dispatch(changeSearchQuery(queryString))
      dispatch(changePagination(1))
      dispatch(changeSearchLabel('all'))
      dispatch(fetchSearchResults())
    },
    onPaginationClick: (pageNumber) => {
      dispatch(changePagination(pageNumber))
      dispatch(fetchSearchResults())
    },
    onLabelClick: (label) => {
      dispatch(changeSearchLabel(label))
      dispatch(changePagination(1))
      dispatch(fetchSearchResults())
    },
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)
