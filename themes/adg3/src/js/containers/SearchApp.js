import React from 'react';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'

import rootReducer from '../reducers/search'
import Search from './Search'

const loggerMiddleware = createLogger()

export function configureStore(initialState) {
  return createStore(
    rootReducer,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}

const store = configureStore()

class SearchApp extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Search />
      </Provider>
    );
  }
};

export default SearchApp
