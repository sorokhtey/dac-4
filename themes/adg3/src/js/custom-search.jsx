import "babel-polyfill"
import React from 'react'
import { render } from 'react-dom'
import SearchApp from './containers/SearchApp'

render(
  <SearchApp/>,
  document.getElementById('custom-search')
)

window.React = require('react')
