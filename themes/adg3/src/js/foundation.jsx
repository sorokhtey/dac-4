require('../sass/app.scss')
import 'script!jquery'
import { foundation } from 'script!foundation-sites/dist/plugins/foundation.core'
// Foundation utility modules
import 'script!foundation-sites/dist/plugins/foundation.util.box'
import 'script!foundation-sites/dist/plugins/foundation.util.keyboard'
import 'script!foundation-sites/dist/plugins/foundation.util.mediaQuery'
import 'script!foundation-sites/dist/plugins/foundation.util.motion'
import 'script!foundation-sites/dist/plugins/foundation.util.nest'
import 'script!foundation-sites/dist/plugins/foundation.util.timerAndImageLoader'
import 'script!foundation-sites/dist/plugins/foundation.util.touch'
import 'script!foundation-sites/dist/plugins/foundation.util.triggers'
// Foundation plugins
import 'script!foundation-sites/dist/plugins/foundation.accordionMenu'
import 'script!foundation-sites/dist/plugins/foundation.drilldown'
import 'script!foundation-sites/dist/plugins/foundation.dropdown'
import 'script!foundation-sites/dist/plugins/foundation.dropdownMenu'
import 'script!foundation-sites/dist/plugins/foundation.offcanvas'
import 'script!foundation-sites/dist/plugins/foundation.orbit'
import 'script!foundation-sites/dist/plugins/foundation.sticky'
import 'script!foundation-sites/dist/plugins/foundation.toggler'
import 'script!foundation-sites/dist/plugins/foundation.magellan'

$(document).ready(function(){
  $(document).foundation();
  var el = new Foundation.DropdownMenu($('.dropdown'), {});
  var elem = new Foundation.Orbit($('#orbit'), {});

  //set wrap before every table
  if (document.getElementById("product-page") != false) {
    $('table').wrap('<div class="table-wrap"></div>');
  }

  /*
    DAC-272 -> Dynamically build an in-page table of contents with the h2 headings 
    and render the TOC in the right column of the page.
  */
  if ($("#TOC")) {
    let links = '';
    let $html = $(`
      <div id="docs-toc">
        <ul class="vertical menu" data-docs-toc data-magellan data-bar-offset="50">
          <li class="title">On this page</li>
        </ul>
      </div>
    `);

    //iterate over the h2 headings in the content body
    $("#content h2").each(function(index, heading) {
      //set the Magellan attribute for the headings tag
      heading.setAttribute('data-magellan-target', heading.id);
      //build the nav anchor links
      links += '<li>' + '<a href="#'+ heading.id +'">'+ heading.textContent + '</a></li>';
    });

    if (links.length) {
      $html.find('ul').append(links);
      $('#TOC').append($html);
      new Foundation.Magellan($('#TOC'));
    }

  }

  /*
    Prevent default submit action to pass the proper search query to the search page
   */
  $('form.search-box-form').on('submit', (e) => {
    e.preventDefault();
    if (!e.target.q.value) return;
    location = e.target.action + '?q=' + e.target.q.value;
  });

  /*
    Adding animation to the search input field on mobile
   */
  {
    let $input = $('#mobile-search > form.search-box-form > input');
    let $resetBtn = $('#mobile-search > form.search-box-form > button.close');
    let $toggleBtn = $('#mobile-search-toggle > .toggle-btn');
    $toggleBtn.on('click', (e) => {
      $input.css({width: 0});
      $input.animate({
        width: '100%'
      }, 500);
      setTimeout(() => $input.focus(), 0);
    });
    $input.on('focusout', (e) => {
      $($input).animate({
        width: 0
      }, 500, () => $resetBtn.click());
    });
  }

  /*
  for correct quick scrolling on our pages task DAC-572
  as we have fixed #subnav-spacer to top we need to scroll offset top to
  #subnav-spacer.height
  */
  $("a[href^='#']").click(function (e) {
    var destination = $(this).attr('href');
    if (destination === "#") {
      return
    }
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $(destination).offset().top - $("#subnav-spacer").height()
    });
  });


  (function checkTouchDevice(){
    if ('ontouchstart' in window){
      $('#top-section-left').hide();
      $(".atlassian-logo").removeClass('hide-for-large');
    };
  })();



  window.ATL_JQ_PAGE_PROPS = $.extend(window.ATL_JQ_PAGE_PROPS, {
    'ee87e716' : {
    "triggerFunction": function(showCollectorDialog) {
        $(".report").click(function(e) {
            e.preventDefault();
            showCollectorDialog();
        });

    },
      "fieldValues": {
        "summary": window.title,
        "customfield_11680": window.location.href
      }
    },
    '52b6b156' : {
    "triggerFunction": function(showCollectorDialog) {
        $(".beta-feedback").click(function(e) {
            e.preventDefault();
            showCollectorDialog();
        });

    },
      "fieldValues": {
        "summary": window.title,
        "customfield_11680": window.location.href
      }
    }
});



      (function ($) {
          var timeouts;
          timeouts = [];
          $('#join-list').on('click', function (e) {
              var emailField = $('#email'),
              email = emailField.val(),
              joinButton = $('#join-list'),
              signupMessage = $('#signup-message');
              if (email !== "" && email.indexOf('@') > -1) {
                  $.post('https://hamlet.atlassian.com/1.0/public/email/' + email + '/subscribe?mailingListId=1243499');
                  emailField.hide();
                  joinButton.hide();
                  signupMessage.html('Thank you for signing up! A confirmation email has been sent.').addClass('success').show(400);
              } else {
                  signupMessage.html('Please enter a valid email address.').addClass('warning').show(400);
              }
          });
          return $('#email').on('keyup', function () {
              var signupMessage, signupMessageVisible;
              signupMessage = $('#signup-message');
              signupMessageVisible = signupMessage.is(':visible');
              if (event.keyCode === 13) {
                  $('#join-list').click();
              } else if (signupMessageVisible) {
                  signupMessage.hide(400).html('');
              }
          });
      }(jQuery));



});
