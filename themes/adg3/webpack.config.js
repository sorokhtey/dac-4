var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'static/js');
var APP_DIR = path.resolve(__dirname, 'src/js')

var config = {
  entry: {
    rssfeed: APP_DIR + '/rssfeed.jsx',
    customsearch: APP_DIR + '/custom-search.jsx',
    foundation: APP_DIR + '/foundation.jsx'
  },
  output: {
    path: path.join(__dirname, 'static'),
    filename: 'js/[name].bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel',
        exclude: /node_modules/,
      },
      { test: /app\.scss$/i, loader: ExtractTextPlugin.extract(['css-loader','sass-loader']) }
    ]
  },
  plugins: [
    new webpack.EnvironmentPlugin(["NODE_ENV",
      "AID_SERVER",
      "AVATAR_SERVER",
      "DAC_RSS_URL",
      "GOOGLE_CSE_API_KEY"]
    ),
    new ExtractTextPlugin("css/dac2-site.css")
    // new webpack.optimize.UglifyJsPlugin({minimize: true}),
  ]
};
module.exports = config;
